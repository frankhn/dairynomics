import { createLocalVue, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'
import IndividualProductPage from '@/pages/IndividualProduct'

const localVue = createLocalVue()
localVue.use(Vuex)

const actions = {
  getIndividualProduct: jest.fn()
}

const store = new Vuex.Store({
  actions
})

const $route = {
  params: {
    productId: 34
  }
}

const wrapper = shallowMount(IndividualProductPage, {localVue,
  store,
  mocks: {
    $route
  }})

describe('IndividualProduct Page Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
