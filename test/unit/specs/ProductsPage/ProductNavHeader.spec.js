import {createLocalVue, shallowMount} from '@vue/test-utils'
import ProductNavHeader from '@/pages/ProductsPage/ProductNavHeader'
import vSelect from 'vue-select'

const localVue = createLocalVue()
localVue.use(vSelect)

const propsData = {
  onCountyChange: jest.fn(),
  allCounties: ['joshua', 'fredrick'],
  selectedCounty: 'joshua'
}

const wrapper = shallowMount(ProductNavHeader, {localVue, propsData})
describe('Product Navigation Bar Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
