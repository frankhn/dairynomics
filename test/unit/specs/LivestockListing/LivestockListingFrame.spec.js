import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import TraverseButton from '@/components/ui/TraverseButton'
import LivestockListingFrame from '@/pages/LivestockListing/LivestockListingFrame.vue'
import vSelect from 'vue-select'

jest.mock('@/store')
let localVue = createLocalVue()
localVue.use(Vuex, vSelect)

const $route = {
  query: {
    page: 5,
    market: 'joshua',
    breed: 'joshua',
    animal: 'joshua'
  }
}

const $router = {
  push: jest.fn()
}

const state = {
  Livestock: {
    isLoading: false,
    livestock: ['joshua', 'fredrick']
  }
}

const mockFunction = jest.fn()
const propsData = {
  livestockListing: [],
  livestockMetaDetails: {},
  countyOptions: [1, 2, 3, 4],
  livestockTypesOptions: [1, 2, 3, 4],
  livestockBreedsOptions: [1, 2, 3, 4]
}

const actions = {
  FETCH_LIVESTOCK: mockFunction,
  SET_CACHED_PAGE: mockFunction,
  FILTER_LIVESTOCK: mockFunction,
  FETCH_LIVESTOCK_BREEDS: mockFunction
}

const store = new Vuex.Store({
  actions, state
})

const wrapper = shallowMount(LivestockListingFrame, {
  localVue,
  store,
  propsData,
  mocks: {
    $route,
    $router
  }
})

describe('Index page for livestock listing', () => {
  it('calls the handleTraversalButtonClick method if the `previous` button is clicked and @clicked is emitted', () => {
    const spy = jest.spyOn(wrapper.vm, 'handleTraversalButtonClick')
    wrapper.find(TraverseButton).vm.$emit('click')
    expect(spy).toHaveBeenCalledTimes(1)
  })
  it('increases the page number if the `next` traversal button is clicked', () => {
    wrapper.setData({ currentPageNumber: 1 })
    wrapper.find('.livestock-next-btn').find(TraverseButton).vm.$emit('click')
    expect(wrapper.vm.currentPageNumber).toEqual(2)
  })
  it('decreases the page number if the `previous` traversal button is clicked', () => {
    wrapper.setData({ currentPageNumber: 20 })
    wrapper.find('.livestock-previous-btn').find(TraverseButton).vm.$emit('click')
    expect(wrapper.vm.currentPageNumber).toEqual(19)
  })
  it('calls the FILTER_LIVESTOCK action when the next button handleTraversalButtonClick method is run', () => {
    wrapper.find('.livestock-next-btn').find(TraverseButton).vm.$emit('click')
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })
  it('calls the FILTER_LIVESTOCK action when the previous button handleTraversalButtonClick method is run', () => {
    wrapper.find('.livestock-previous-btn').find(TraverseButton).vm.$emit('click')
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })

  it(`calls the setSelected method when livestock type selected vSelect component
    triggers a change event`, () => {
    const setLivestockSpy = jest.spyOn(wrapper.vm, 'setSelected')
    setLivestockSpy({
      filterType: 'animal',
      filterValue: 'beef'
    })
    expect(wrapper.vm.selectedAnimal).toEqual('beef')
    expect(actions.FETCH_LIVESTOCK_BREEDS).toHaveBeenCalled()
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })
  it(`calls the setSelected method when livestock type selected vSelect component
    triggers a change event and the filter value is 'All-Types`, () => {
    const setLivestockSpy = jest.spyOn(wrapper.vm, 'setSelected')
    setLivestockSpy({
      filterType: 'animal',
      filterValue: 'All-Types'
    })
    expect(wrapper.vm.selectedAnimal).toEqual('')
    expect(actions.FETCH_LIVESTOCK_BREEDS).toHaveBeenCalled()
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })
  it(`calls the setSelected method when livestock breed selected on vSelect component
    triggers a change event`, () => {
    const setLivestockSpy = jest.spyOn(wrapper.vm, 'setSelected')
    setLivestockSpy({
      filterType: 'breed',
      filterValue: 'Friesian'
    })
    expect(wrapper.vm.selectedBreed).toEqual('Friesian')
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })

  it(`calls the setSelected method when livestock breed selected vSelect component
    triggers a change event and the filter value is 'All-Breeds'`, () => {
    const setLivestockSpy = jest.spyOn(wrapper.vm, 'setSelected')
    setLivestockSpy({
      filterType: 'breed',
      filterValue: 'All-Breeds'
    })
    expect(wrapper.vm.selectedBreed).toEqual('')
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })
  it(`calls the setSelected method when livestock county selected vSelect component
    triggers a change event and the filter value is 'All-Counties'`, () => {
    const setLivestockSpy = jest.spyOn(wrapper.vm, 'setSelected')
    setLivestockSpy({
      filterType: 'market',
      filterValue: 'All-Counties'
    })
    expect(wrapper.vm.selectedMarket).toEqual('')
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })
  it(`calls the setSelected method when livestock county selected vSelect component
    triggers a change event and the filter value is not 'All-Counties'`, () => {
    const setLivestockSpy = jest.spyOn(wrapper.vm, 'setSelected')
    setLivestockSpy({
      filterType: 'market',
      filterValue: 'Bungoma'
    })
    expect(wrapper.vm.selectedMarket).toEqual('Bungoma')
    expect(actions.FILTER_LIVESTOCK).toHaveBeenCalled()
  })
})
