import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import AxiosCalls from '@/utils/api/AxiosCalls.js'

jest.mock('@/router/router')
const mock = new MockAdapter(axios)

describe('Axios Api Calls', () => {
  afterEach(() => {
    mock.reset()
  })
  it('calls the right api when there\'s no error', (done) => {
    mock.onGet('https://beta.cowsoko.com/api/v1/blogs').reply(200, {
      blogs: [
        { id: 1, title: 'Test title', body: 'Test body' }
      ]
    })
    AxiosCalls.get('api/v1/blogs').then((response) => {
      expect(response.data.blogs).toEqual([{ id: 1, title: 'Test title', body: 'Test body' }])
      done()
    })
  })
})
