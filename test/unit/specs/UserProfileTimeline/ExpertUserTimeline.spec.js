import Vuex from 'vuex'
import { createLocalVue, shallowMount } from '@vue/test-utils'
import ExpertProfile from '@/pages/UserProfileTimeline/UserProfileTimeline.vue'
import mockUpdates from '@/store/modules/mocks/updates'
import Vuelidate from 'vuelidate'

jest.mock('@/router/router')

const localVue = createLocalVue()
localVue.use(Vuelidate)
localVue.use(Vuex)
describe('UserProfile.vue', () => {
  let wrapper
  let actions
  let store
  let getters
  let state
  let isLoading = true
  let isFetching = true
  let expertUpdates = []
  const mountComponent = () => {
    actions = {
      editUser: jest.fn(),
      userDetails: jest.fn(),
      UPDATE_EXPERT_UPDATES: jest.fn(),
      GET_EXPERT_UPDATES: jest.fn()
    }

    state = {
      ExpertProfile: {
        isLoading
      },
      user: {},
      expertUpdates,
      numberOfExpertUpdates: undefined,
      isLoading: false,
      page: { currentPage: 1, lastPage: 2 },
      isFetching
    }

    getters = {
      USER_PROFILE: expertState => expertState.user,
      getExpertUpdatesPage: expertState => expertState.page,
      getExpertUpdates: expertState => expertState.expertUpdates,
      getIsFetching: expertState => expertState.isFetching,
      getNumberOfExpertUpdates: expertState => expertState.numberOfExpertUpdates
    }
    store = new Vuex.Store({
      state,
      actions,
      getters
    })
    return shallowMount(ExpertProfile, { store, localVue })
  }

  it('should render after fetching data', () => {
    isFetching = false
    isLoading = false
    expertUpdates = mockUpdates

    wrapper = mountComponent()
    wrapper.vm.USER_PROFILE = {
      id: 123,
      is_expert: '1'
    }

    let button = wrapper.find('.btn.big-full')
    button.trigger('click')
    expect(actions.UPDATE_EXPERT_UPDATES).toHaveBeenCalled()
  })
})
