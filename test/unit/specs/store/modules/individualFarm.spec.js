import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import individualFarmModule from '@/store/modules/individualFarm.js'

const router = {
  push: jest.fn()
}

jest.mock('@/store')
const mock = new AxiosMockAdapter(axios)

describe('individualFarm Module Test', () => {
  it('state should be equal to payload', () => {
    let payload = [ {id: '122', 'title': 'ddsdsd', viewed: '0'} ]
    individualFarmModule.mutations.farmDetails(individualFarmModule.state, payload)
    expect(individualFarmModule.state.farmDetails).toBe(payload)
  })

  it('state.isLoading should be false', () => {
    individualFarmModule.mutations.loading(individualFarmModule.state, false)
    expect(individualFarmModule.state.isLoading).toBe(false)
  })

  it('should get farm Details', () => {
    individualFarmModule.getters.farmInfo(individualFarmModule.state)
    expect(individualFarmModule.state.farmDetails.length).toBe(1)
  })

  it('should fetch individual Farm', () => {
    const commit = jest.fn()
    const resp = {
      success: true,
      name: 'test'
    }

    mock.onGet('https://beta.cowsoko.com/api/v1/farms/2').reply(200, resp)

    individualFarmModule.actions.getFarmDetails({commit}, {farmId: 2, router}).then(() => {
      expect(commit).toHaveBeenCalledWith('loading')
      expect(individualFarmModule.getters.farmInfo.farmDetails.name).toEqual('test')
    })
  })

  it('should redirect to 404 page for an unknown farm id', () => {
    const commit = jest.fn()
    const resp = {
      success: false,
      photo: ['https://joshua.jpg']
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/farms/2').reply(404, resp)

    individualFarmModule.actions.getFarmDetails({commit}, {farmId: 2, router}).then(() => {
      expect(router.push).toHaveBeenCalledWith('/404')
    })
  })

  it('should redirect to home  for other status code from the server', () => {
    const commit = jest.fn()
    const resp = {
      success: false
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/farms/2').reply(403, resp)

    individualFarmModule.actions.getFarmDetails({commit}, {farmId: 2, router}).then(() => {
      expect(router.push).toHaveBeenCalledWith('/')
    })
  })
})
