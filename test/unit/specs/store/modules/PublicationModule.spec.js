import axios from 'axios'
import IndivualPublicationModule from '@/store/modules/IndivualPublicationModule'
import MockAdapter from 'axios-mock-adapter'
import { mock } from '@/store/modules/mocks/mockInstance'
import mockPublications from '@/store/modules/mocks/publications'

jest.mock('@/store')

const state = {
  publicationId: '',
  publication: [],
  error: '',
  download: '',
  loading: false,
  isLoading: true
}

const mocked = new MockAdapter(axios)
describe('publication module mutations', () => {
  it('test VIEW_SINGLE_PUBLICATION mutation', () => {
    const publication = {
      id: 1
    }
    IndivualPublicationModule.mutations.VIEW_SINGLE_PUBLICATION(state, publication)
    IndivualPublicationModule.mutations.IS_PAGE_LOADING(state, false)
    expect(state.publication).toEqual(publication)
    expect(state.isLoading).toEqual(false)
  })
  it('test PUBLICATIOn_ERROR mutation', () => {
    const error = {
      error: 'network error'
    }
    IndivualPublicationModule.mutations.PUBLICATION_ERROR(state, error)
    expect(state.error).toEqual(error)
  })
  it('test DOWNLOAD_PUBLICATION and LOADING_ICON mutation', () => {
    const downloadObj = {
      status: 200,
      size: 49
    }
    IndivualPublicationModule.mutations.DOWNLOAD_PUBLICATION(state, downloadObj)
    expect(state.download).toEqual(downloadObj)
    IndivualPublicationModule.mutations.LOADING_ICON(state, true)
    expect(state.loading).toEqual(true)
  })
  it('test GETTERS', () => {
    IndivualPublicationModule.getters.publicationGetter(state.publication)
    IndivualPublicationModule.getters.downloadGetter(state.download)
    IndivualPublicationModule.getters.downloadLoader(state.loading)
    IndivualPublicationModule.getters.downloadError(state.error)
    IndivualPublicationModule.getters.isLoading(state.isLoading)
    expect(state).toEqual(state)
  })
})

describe('test single publication actions', () => {
  afterEach(() => {
    mock.reset()
  })
  beforeEach(() => {
    // jwt_decode = jest.fn()
  })

  it('should get a single publication', (done) => {
    const commit = jest.fn()

    const config = {
      method: 'GET',
      headers: {
        Authorization: `Bearer mocked-token`
      },
      data: {user_id: 1886},
      responseType: 'blob'
    }
    mocked.onGet('https://beta.cowsoko.com/api/v1/publications/1', config).reply(200, {success: true})
    IndivualPublicationModule.actions.singlePublication({commit}, 1).then(() => {
      expect(mocked.history.get[0].url).toEqual('https://beta.cowsoko.com/api/v1/publications/1')
      done()
    })
  })

  it('should not get a single publication', () => {
    const commit = jest.fn()

    const config = {
      method: 'GET',
      headers: {
        Authorization: `Bearer mocked-token`
      },
      data: {user_id: 1886},
      responseType: 'blob'
    }
    mocked.onGet('https://beta.cowsoko.com/api/v1/publications/1', config).reply(200, { success: false })
    IndivualPublicationModule.actions.singlePublication({commit}, 1).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })

  it('should download publication', () => {
    const commit = jest.fn()
    const config = {
      method: 'GET',
      headers: {
        Authorization: `Bearer mocked-token`
      },
      data: {user_id: 1886},
      responseType: 'blob'
    }
    window.URL.createObjectURL = jest.fn()
    const createElementSpy = jest.spyOn(document, 'createElement')
    mocked.onGet('https://beta.cowsoko.com/api/v1/publications/3/download', config).reply(200, 'joshua')
    IndivualPublicationModule.actions.downloadPublication({commit}, mockPublications[0]).then(() => {
      expect(commit).toHaveBeenCalled()
      expect(window.URL.createObjectURL).toHaveBeenCalled()
      expect(createElementSpy).toHaveBeenCalled()
    })
  })

  it('should not download publication when response is not 200', () => {
    const commit = jest.fn()
    const config = {
      method: 'GET',
      headers: {
        Authorization: `Bearer mocked-token`
      },
      data: {user_id: 1886},
      responseType: 'blob'
    }
    mocked.onGet('https://beta.cowsoko.com/api/v1/publications/3/download', config).reply(202, 'joshua')
    IndivualPublicationModule.actions.downloadPublication({commit}, mockPublications[0]).then(() => {
      expect(commit).toHaveBeenCalled()
    })
  })
})
