import AllBlogs from '@/store/modules/AllBlogs'
import { mock } from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')

afterEach(() => {
  mock.reset()
})

describe('all blogs store module', () => {
  describe('All blogs mutation', () => {
    describe('SET_BLOGS mutation', () => {
      const state = { blogs: [] }
      it('it should set the blogs', () => {
        AllBlogs.mutations.SET_BLOGS(state, ['blogs1', 'blogs2'])
        expect(state).toEqual({ blogs: ['blogs1', 'blogs2'] })
      })
    })

    describe('SET_PAGINATED_BLOGS mutation', () => {
      const state = { paginatedblogs: [] }
      it('it should add blogs to paginated array', () => {
        AllBlogs.mutations.SET_PAGINATED_BLOGS(state, {data: 'blogs'})
        expect(state).toEqual({ paginatedblogs: [{data: 'blogs'}] })
      })
    })

    describe('SET_LOADER mutation', () => {
      const state = { isLoading: false }
      it('it should set the loader to true', () => {
        AllBlogs.mutations.SET_LOADER(state, true)
        expect(state).toEqual({isLoading: true})
      })
    })
  })

  describe('SET_CURRECT_PAGE mutation', () => {
    it('should set currentpage to current page', () => {
      const state = {
        currentpage: null
      }
      AllBlogs.mutations.SET_CURRENT_PAGE(state, 2)
      expect(state.currentpage).toEqual(2)
    })
  })

  describe('CLEAR_PAGINATED_BLOGS mutation', () => {
    it('should set state.paginatedblogs to an empty array', () => {
      const state = {
        currentpage: 2,
        paginatedblogs: ['joshua', 'fredrick']
      }
      AllBlogs.mutations.CLEAR_PAGINATED_BLOGS(state)
      expect(state.paginatedblogs.length).toBeFalsy()
    })
  })

  describe('All Blogs getter', () => {
    let state
    beforeEach(() => {
      state = {
        blogs: ['blog', 'blog2']
      }
    })

    describe('getBlogs getter', () => {
      it('it should return all blogs', () => {
        let results = AllBlogs.getters.getBlogs(state)
        expect(results).toEqual({ blogs: ['blog', 'blog2'] })
      })
    })
  })

  describe('All Blogs actions', () => {
    let commit
    beforeEach(() => {
      commit = jest.fn()
    })

    it('should make an api call if blogs are empty', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/blogs?page=1').reply(200, 'joshua')
      let state = {
        blogs: [],
        paginatedblogs: [],
        currentpage: 1,
        isLoading: false
      }
      AllBlogs.actions.getAllBlogs({commit, state}, 1)
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit).toHaveBeenCalledWith('SET_LOADER', true)
    })

    it('should commit set_paginated_blogs when setData is set to true', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/blogs?page=2').reply(200, 'joshua')
      let state = {
        blogs: ['blogs2'],
        paginatedblogs: ['blogs', 'blogs2'],
        isLoading: false,
        currentpage: 5
      }
      AllBlogs.actions.getAllBlogs({ commit, state }, 2)
      expect(commit).toHaveBeenCalledTimes(1)
    })

    it('should commit set_blogs if state.blogs is not zero and pagenumber is not equal to state.paginatedblogs.length', () => {
      const state = {
        blogs: ['joshua', 'fredrick'],
        paginatedblogs: ['joshua', 'fredrick', 'david', 'peace'],
        currentpage: 1
      }
      AllBlogs.actions.getAllBlogs({commit, state}, 2)
      expect(commit).toHaveBeenCalled()
    })
  })
})
