import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import sinon from 'sinon'
import allUpdatesModule from '@/store/modules/allUpdatesModule'
import mockUpdates from '@/store/modules/mocks/updates'

jest.mock('@/store')
jest.mock('@/router/router')

describe('Updates module', () => {
  const state = {
    mainUpdates: {
      allUpdates: [],
      expertsUpdates: {
        'expert-id': []
      },
      requestStatuses: {
        isFetching: false
      },
      currentPage: 1
    }
  }
  let expertId = 1

  describe('Mutations on the updates module', () => {
    it('handles STORE_FETCHED_UPDATES mutation', () => {
      expect(state.mainUpdates.allUpdates).toEqual([])
      allUpdatesModule.mutations.STORE_FETCHED_UPDATES(
        state, {
          data: mockUpdates
        }
      )
      expect(state.mainUpdates.allUpdates).toEqual(mockUpdates)
      allUpdatesModule.mutations.STORE_FETCHED_UPDATES(
        state, {
          data: mockUpdates
        }
      )
      expect(state.mainUpdates.allUpdates).toEqual(mockUpdates)
    })

    it('handles STORE_FETCHED_EXPERT_UPDATES mutation', () => {
      allUpdatesModule.mutations.STORE_FETCHED_EXPERT_UPDATES(
        state, {
          data: mockUpdates,
          expertId
        }
      )
      expect(state.mainUpdates.expertsUpdates[expertId]).toEqual(mockUpdates)
    })

    it('handles CHANGE_REQUEST_STATUS mutation', () => {
      const isFetching = !state.mainUpdates.requestStatuses.isFetching
      allUpdatesModule.mutations.CHANGE_REQUEST_STATUS(
        state, {
          isFetching
        }
      )
      expect(state.mainUpdates.requestStatuses.isFetching).toEqual(isFetching)
    })

    it('handles CHANGE_CURRENT_PAGE_NUMBER mutation', () => {
      const currentPage = state.mainUpdates.currentPage
      allUpdatesModule.mutations.CHANGE_CURRENT_PAGE_NUMBER(
        state, {
          currentPage
        }
      )
      expect(state.mainUpdates.currentPage).toEqual(1)
    })
  })

  describe('Getters on the updates modules', () => {
    it('handles getAllUpdates() getter', () => {
      const {
        getters: {
          getAllUpdates
        }
      } = allUpdatesModule
      const updates = getAllUpdates(state)
      expect(updates).toBe(state.mainUpdates.allUpdates)
    })
    it('handles getExpertsUpdates() getter', () => {
      const {
        getters: {
          getExpertsUpdates
        }
      } = allUpdatesModule
      const updates = getExpertsUpdates(state)
      expect(updates).toBe(state.mainUpdates.expertsUpdates)
    })
    it('handles getUpdatesRequestStatuses() getter', () => {
      const {
        getters: {
          getUpdatesRequestStatuses
        }
      } = allUpdatesModule
      const requestStatuses = getUpdatesRequestStatuses(state)
      expect(requestStatuses).toBe(state.mainUpdates.requestStatuses)
    })
    it('handles getCurrentPage() getter', () => {
      const {
        getters: {
          getCurrentPage
        }
      } = allUpdatesModule
      const currentPage = getCurrentPage(state)
      expect(currentPage).toBe(state.mainUpdates.currentPage)
    })
  })

  describe('Actions on the updates module', () => {
    let mock = new AxiosMockAdapter(axios)

    const mockedResponse = {
      data: {
        data: { updates: mockUpdates }
      },
      meta: { current_page: 1 }
    }

    it('handles fetchUpdates action', async () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/updates?page=1')
        .replyOnce(200, mockedResponse)

      let commit = sinon.spy()
      const state = {
        mainUpdates: {
          allUpdates: []
        }
      }

      const {
        actions: {
          fetchUpdates
        }
      } = allUpdatesModule
      await fetchUpdates({
        commit,
        state
      }, {})

      expect(commit.args).toEqual([
        ['CHANGE_REQUEST_STATUS', {
          isFetching: true
        }],
        ['CHANGE_REQUEST_STATUS', {
          isFetching: false
        }],
        ['CHANGE_CURRENT_PAGE_NUMBER', { currentPage: mockedResponse.meta.current_page }],
        ['STORE_FETCHED_UPDATES', {
          data: mockedResponse.data.updates
        }]
      ])

      // handle case where fetchUpdate is called with dataScope = expertUpdates
      const expertId = 1
      mock.onGet(`https://beta.cowsoko.com/api/v1/experts/${expertId}/updates?page=1`)
        .replyOnce(200, mockedResponse.data)

      commit = sinon.spy()
      await fetchUpdates({
        commit,
        state
      }, {
        pageNumber: 1,
        dataScope: 'expertUpdates',
        expertId
      })
      expect(commit.args.length).toEqual(3)
    })

    it('throws an error when there is a network error', async () => {
      const {
        actions: {
          fetchUpdates
        }
      } = allUpdatesModule

      const response = {
        success: false,
        error: 'Some error'
      }

      // handle case where fetchUpdate is called with dataScope = expertUpdates
      const expertId = 1
      mock.onGet(`https://beta.cowsoko.com/api/v1/experts/${expertId}/updates?page=1`)
        .replyOnce(404, response)

      const commit = sinon.spy()
      await fetchUpdates({ commit, state }, {
        pageNumber: 1,
        dataScope: 'expertUpdates',
        expertId
      }).catch((response) => {
        // expect(commit.args.length).toEqual(2)

        expect(commit.args).toEqual([
          ['CHANGE_REQUEST_STATUS', {
            isFetching: true
          }],
          ['CHANGE_REQUEST_STATUS', {
            isFetching: false
          }]
        ])
      })
    })
  })
})
