import expertList from '@/store/modules/expertList'
import AxiosCalls from '@/utils/api/AxiosCalls'
import router from '@/router/router'
import * as helperFunctions from '@/utils/helperFunctions'

jest.mock('@/utils/helperFunctions')
jest.mock('@/store')

describe('experList store module', () => {
  describe('expertList mutations', () => {
    describe('TOGGLE_IS_LOADING mutation', () => {
      const state = { isLoading: false }
      it('should toggle "state.isLoading" to true', () => {
        expertList.mutations.TOGGLE_IS_LOADING(state, true)
        expect(state).toEqual({ isLoading: true })
      })

      it('should toggle "state.isLoading" to false', () => {
        expertList.mutations.TOGGLE_IS_LOADING(state, false)
        expect(state).toEqual({ isLoading: false })
      })
    })

    describe('SET_SELECTED_COUNTY mutation', () => {
      it('should set "state.selected"', () => {
        const state = { selected: null }
        expertList.mutations.SET_SELECTED_COUNTY(state, 1)
        expect(state).toEqual({ selected: 1 })
      })
    })

    describe('SET_CURRENT_PAGE mutation', () => {
      it('should set "state.currentPageNumber"', () => {
        const state = { currentPageNumber: 0 }
        expertList.mutations.SET_CURRENT_PAGE(state, 3)
        expect(state).toEqual({ currentPageNumber: 3 })
      })
    })

    describe('SET_COUNTIES mutation', () => {
      it('should set "state.counties"', () => {
        const state = { counties: [], selected: 4 }
        expertList.mutations.SET_COUNTIES(state, [1, 2])
        expect(state).toEqual({ counties: [4, 1, 2], selected: 4 })
      })
    })

    describe('UPDATE_EXPERTS mutation', () => {
      it('should updates "state.experts if "state.selected.id" exist as key "state.experts', () => {
        const state = { experts: { }, selected: { id: 4 }, currentPageNumber: 2 }
        const data = {
          selectedCounty: {
            id: 4
          },
          data: ['a']
        }
        expertList.mutations.UPDATE_EXPERTS(state, data)
        expect(state).toEqual({ experts: { 4: {2: ['a']} }, selected: { id: 4 }, currentPageNumber: 2 })
      })
    })
  })

  describe('expertList getter', () => {
    let state
    beforeEach(() => {
      state = {
        experts: { 4: [{}, 'A'] },
        selected: { id: 4 },
        currentPageNumber: 1
      }
    })
    describe('currentPage getter', () => {
      it('should return the currentPage; "A"', () => {
        const result = expertList.getters.currentPage(state)
        expect(result).toEqual('A')
      })
    })
  })

  describe('expertList actions', () => {
    let commit
    beforeEach(() => {
      commit = jest.fn()
    })

    describe('getCounties action', () => {
      it('should not make API call if "state.counties" is not empty', () => {
        const state = { counties: [{ county: 'Bomet' }] }

        expertList.actions.getCounties({ commit, state })
        expect(commit).toHaveBeenCalledTimes(0)
      })

      it('should make API call if "state.counties" is empty', async () => {
        const counties = [
          { county: 'Baringo' },
          { county: 'Bomet' }
        ]
        AxiosCalls.awaitGet = jest.fn(() => ({ counties }))

        const state = { counties: [] }

        await expertList.actions.getCounties({ commit, state })

        expect(commit).toHaveBeenCalledTimes(1)
        expect(commit).toHaveBeenLastCalledWith('SET_COUNTIES', counties)
      })

      it('should alert an error if the API returns an error', async () => {
        const errorObj = { error: 404 }
        helperFunctions.alertError = jest.fn()

        AxiosCalls.awaitGet = jest.fn(() => errorObj)

        const state = { counties: [] }

        await expertList.actions.getCounties({ commit, state })

        expect(helperFunctions.alertError).toHaveBeenCalledTimes(1)
        expect(commit).toHaveBeenCalledTimes(0)
      })
    })

    describe('getExpertCounties action', () => {
      let dispatch
      beforeEach(() => {
        dispatch = jest.fn()
      })

      it('should dispatch "getCounties" and "getExperts"', async () => {
        const state = { counties: [{ county: 'Bomet' }] }

        await expertList.actions.getExpertCounties({ commit, state, dispatch }, 'bomet')
        expect(commit).toHaveBeenCalledTimes(1)
        expect(commit).toHaveBeenCalledWith('TOGGLE_IS_LOADING', true)
        expect(dispatch).toHaveBeenCalledTimes(2)
      })

      it('should redirect to "/experts/county" if second param is not supplied', async () => {
        router.push = jest.fn()
        const state = { counties: [{ county: 'Bomet' }] }

        await expertList.actions.getExpertCounties({ commit, state, dispatch })
        expect(dispatch).toHaveBeenCalledWith('getExperts', { 'selectedCounty': undefined })
        expect(router.push).toHaveBeenCalledWith(router.history.current.fullPath)
      })
    })

    describe('getExperts action', () => {
      let getters
      beforeEach(() => {
        getters = { expertListIsCached: jest.fn() }
      })

      it('should make API call to get all experts from a specified county', async () => {
        AxiosCalls.awaitGet = jest.fn((url, page) => ({ meta: { current_page: 1 } }))

        const state = { experts: { 1: [{}, 'A'] }, defaultCounty: { id: 4 } }

        await expertList.actions.getExperts({ commit, state, getters }, {})
        expect(commit).toHaveBeenCalledTimes(5)
        expect(AxiosCalls.awaitGet).toBeCalledWith(`experts/county/${state.defaultCounty.id}?page=1`)
      })

      it('should make API call to get all experts', async () => {
        AxiosCalls.awaitGet = jest.fn((url, page) => ({ meta: { current_page: 1 } }))

        const state = { experts: { 1: [{}, 'A'] }, defaultCounty: { id: 0 } }

        await expertList.actions.getExperts({ commit, state, getters }, {})
        expect(commit).toHaveBeenCalledTimes(5)
        expect(AxiosCalls.awaitGet).toBeCalledWith('experts?page=1')
      })

      it('should alert an error if the API returns an error', async () => {
        const errorObj = { error: 404 }
        helperFunctions.alertError = jest.fn()

        AxiosCalls.awaitGet = jest.fn(() => errorObj)

        const state = { experts: { 1: [{}, 'A'] }, defaultCounty: { id: 0 } }

        await expertList.actions.getExperts({ commit, state, getters }, {})

        expect(helperFunctions.alertError).toHaveBeenCalledTimes(1)
      })

      it('should not alert error if API returns 401 error', async () => {
        const errorObj = { error: 401 }
        helperFunctions.alertError = jest.fn()
        AxiosCalls.awaitGet = jest.fn(() => errorObj)

        const state = { experts: { 1: [{}, 'A'] }, defaultCounty: { id: 4 } }

        await expertList.actions.getExperts({ commit, state, getters }, {})
        expect(helperFunctions.alertError).toHaveBeenCalledTimes(0)
      })

      it('should make API call to get all experts in a specific pagination page', async () => {
        AxiosCalls.awaitGet = jest.fn((url, page) => ({ meta: { current_page: 1 } }))
        const page = { pageNumber: 3 }
        const state = { experts: { 1: [{}, 'A'] }, defaultCounty: { id: 0 } }

        await expertList.actions.getExperts({ commit, state, getters }, page)
        expect(commit).toHaveBeenCalledTimes(5)
        expect(AxiosCalls.awaitGet).toBeCalledWith(`experts?page=${page.pageNumber}`)
      })
    })
  })
})
