import axios from 'axios'
import AxiosMockAdapter from 'axios-mock-adapter'
import IndividualProductModule from '@/store/modules/IndividualProductModule.js'
import mockPayload from '@/store/modules/mocks/individualProductModule'

jest.mock('@/store')

jest.mock('@/router/router')
const mock = new AxiosMockAdapter(axios)

describe('IndividualProductModule Test', () => {
  it('state should be equal to payload', () => {
    IndividualProductModule.mutations.INDIVIDUAL_PRODUCT_DETAILS(IndividualProductModule.state, mockPayload)
    expect(Object.keys(IndividualProductModule.state.IndividualProductDetails)).toHaveLength(9)
  })

  it('should get individualproducts', () => {
    IndividualProductModule.getters.getIndividualProduct(IndividualProductModule.state)
    expect(IndividualProductModule.state.IndividualProductDetails).toBeTruthy()
  })

  it('should fetch individual products', () => {
    const context = {
      commit: jest.fn()
    }

    mock.onGet('https://beta.cowsoko.com/api/v1/products/4').reply(200, mockPayload)

    IndividualProductModule.actions.getIndividualProduct(context, 4).then(() => {
      expect(context.commit).toHaveBeenCalled()
      expect(IndividualProductModule.state.IndividualProductDetails.description).toEqual('Net Ninja')
    })
  })

  it('should redirect to 404 when server responds with error', () => {
    const context = {
      commit: jest.fn()
    }

    const response = {
      error: 404
    }
    mock.onGet('https://beta.cowsoko.com/api/v1/products/4').reply(200, response)

    IndividualProductModule.actions.getIndividualProduct(context, 4).then(() => {
      expect(context.commit).not.toHaveBeenCalled()
    })
  })
})
