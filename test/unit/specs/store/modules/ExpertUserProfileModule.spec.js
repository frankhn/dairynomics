import ExpertUserProfileModule from '@/store/modules/ExpertUserProfileModule'
import { mock } from '@/store/modules/mocks/mockInstance'

const commit = jest.fn()
jest.mock('@/store')

afterEach(() => {
  mock.reset()
})

describe('Expert User Profile module Test', () => {
  describe('Expert user profile action test', () => {
    it('expertProfile should call commit', () => {
      const response = {
        data: []
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/experts/54').reply(200, response)
      ExpertUserProfileModule.actions.expertProfile({commit}, 54)
        .then(() => {
          expect(commit).toHaveBeenCalled()
        })
    })

    it('expertprofile should commit profile errors  when api calls returns error', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/experts/32').reply(400)
      ExpertUserProfileModule.actions.expertProfile({commit}, 32)
        .then({}).catch(() => {
          expect(commit).toHaveBeenCalled()
        })
    })

    it('expertFarms should commit SET_EXPERT_FARMS', () => {
      const response = {
        data: 'joshua'
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/experts/20/farms').reply(200, response)
      ExpertUserProfileModule.actions.expertFarms({commit}, 20).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('expertFarms should commit SET_FARMS_ERROR when api call returns error', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/experts/20/farms').reply(400)
      ExpertUserProfileModule.actions.expertFarms({commit}, 20)
        .then({}).catch(() => {
          expect(commit).toHaveBeenCalled()
        })
    })

    it('expertBlogs should commit SET_EXPERT_BLOGS', () => {
      const response = {
        data: 'joshua'
      }
      mock.onGet('https://beta.cowsoko.com/api/v1/experts/20/blogs').reply(200, response)
      ExpertUserProfileModule.actions.expertBlogs({commit}, 20).then(() => {
        expect(commit).toHaveBeenCalled()
      })
    })

    it('expertBlogs should commit SET_BLOGS_ERROR when api call returns error', () => {
      mock.onGet('https://beta.cowsoko.com/api/v1/experts/20/blogs').reply(400)
      ExpertUserProfileModule.actions.expertBlogs({commit}, 20)
        .then({}).catch(() => {
          expect(commit).toHaveBeenCalled()
        })
    })
  })

  describe('Expert user profile mutation test', () => {
    it('GetExpertProfile set getexpertprofile to user', () => {
      const state = {}
      ExpertUserProfileModule.mutations.GetExpertProfile(state, 'joshua')
      expect(state.getExpertProfile).toEqual('joshua')
    })

    it('ProfileErrors should set expertprofileError to errorMessage', () => {
      const state = {}
      ExpertUserProfileModule.mutations.ProfileErrors(state, 'there is error')
      expect(state.ExpertProfileError).toEqual('there is error')
    })

    it('SET_EXPERT_FARMS should set setExpertFarms to farm', () => {
      const state = {}
      ExpertUserProfileModule.mutations.SET_EXPERT_FARMS(state, 'joshua_farm')
      expect(state.setExpertFarms).toEqual('joshua_farm')
    })

    it('SET_FARMS_ERROR should set expertFarmError to error', () => {
      const state = {}
      ExpertUserProfileModule.mutations.SET_FARMS_ERROR(state, 'there is error')
      expect(state.expertFarmError).toEqual('there is error')
    })

    it('SET_EXPERT_BLOGS should set setExpertBlogs to blog', () => {
      const state = {}
      ExpertUserProfileModule.mutations.SET_EXPERT_BLOGS(state, 'blog')
      expect(state.setExpertBlogs).toEqual('blog')
    })

    it('SET_BLOGS_ERROR should set expertBlogError to error', () => {
      const state = {}
      ExpertUserProfileModule.mutations.SET_BLOGS_ERROR(state, 'there is error')
      expect(state.expertBlogError).toEqual('there is error')
    })
  })

  describe('Expert user profile module getters test', () => {
    it('should getExpertProfileInformation', () => {
      const state = 'joshua'
      const expectedResult = ExpertUserProfileModule.getters.getExpertProfileInformation(state)
      expect(expectedResult).toEqual('joshua')
    })

    it('getExpertFarms shuold return state.setExpertFarms', () => {
      const state = {
        setExpertFarms: 'joshua'
      }
      const expectedResult = ExpertUserProfileModule.getters.getExpertFarms(state)
      expect(expectedResult).toEqual('joshua')
    })

    it('getExpertFarmsError should return state.expertFarmError', () => {
      const state = {
        expertFarmError: 'error'
      }
      const expectedResult = ExpertUserProfileModule.getters.getExpertFarmsError(state)
      expect(expectedResult).toEqual('error')
    })

    it('getExpertBlogs should return setExpertBlogs', () => {
      const state = {
        setExpertBlogs: 'joshua'
      }
      const expectedResult = ExpertUserProfileModule.getters.getExpertBlogs(state)
      expect(expectedResult).toEqual('joshua')
    })

    it('getExpertBlogsError should return expertBlogError', () => {
      const state = {
        expertBlogError: 'error'
      }
      const expectedResult = ExpertUserProfileModule.getters.getExpertBlogsError(state)
      expect(expectedResult).toEqual('error')
    })
  })
})
