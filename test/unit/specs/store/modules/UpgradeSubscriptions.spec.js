import UpgradeSubscription from '@/store/modules/UpgradeSubscription'
import { mock } from '@/store/modules/mocks/mockInstance'

jest.mock('@/store')

afterEach(() => {
  mock.reset()
})

describe('upgrade subscription module', () => {
  describe('upgrade subscription mutation', () => {
    describe('SET_LOADER mutation', () => {
      const state = {
        subscription: [],
        isLoading: false
      }
      it('it should set the loader', () => {
        UpgradeSubscription.mutations.SET_LOADER(state, true)
        expect(state).toEqual({ subscription: [], isLoading: true })
      })
    })

    describe('SET_MESSAGE mutation', () => {
      const state = {
        isLoading: false,
        errorMessage: ''
      }
      it('it should set the error message', () => {
        UpgradeSubscription.mutations.SET_MESSAGE(state, 'something went wrong')
        expect(state).toEqual({ isLoading: false, errorMessage: 'something went wrong' })
      })
    })

    describe('SET_PAYEMENT_STATUS mutation', () => {
      const state = {
        isLoading: false,
        errorMessage: '',
        paymentStatus: ''
      }
      it('it should set the error payment status', () => {
        UpgradeSubscription.mutations.SET_PAYEMENT_STATUS(state, 'failure')
        expect(state).toEqual({ isLoading: false, errorMessage: '', paymentStatus: 'failure' })
      })
    })
  })

  describe('upgrade subscription getters', () => {
    describe('getSubscriptionInfo getter', () => {
      const state = {
        subscription: ['bronze package'],
        isLoading: false
      }
      it('it should be able get the current state of a component', () => {
        let results = UpgradeSubscription.getters.getSubscriptionInfo(state)
        expect(results).toEqual({ subscription: ['bronze package'], isLoading: false })
      })
    })

    describe('getErrorMessage getter', () => {
      const state = {
        isLoading: false,
        errorMessage: 'something went wrong'
      }
      it('it should be able get the current state of a component', () => {
        let results = UpgradeSubscription.getters.getErrorMessage(state)
        expect(results).toEqual('something went wrong')
      })
    })

    describe('getPaymentStatus getter', () => {
      const state = {
        isLoading: false,
        paymentStatus: 'success'
      }
      it('it should be able get the current state of a component', () => {
        let results = UpgradeSubscription.getters.getPaymentStatus(state)
        expect(results).toEqual('success')
      })
    })
  })

  describe('upgrade subscription actions', () => {
    describe('makeSubscription action', () => {
      let commit
      beforeEach(() => {
        commit = jest.fn()
      })

      it('it should make an API call to initiate payment', () => {
        UpgradeSubscription.actions.makeSubscription({ commit }, {contact: '256700500800', amount: '500', month: '3'})
        expect(commit).toHaveBeenCalled()
      })
    })

    describe('resetMessage action', () => {
      let commit
      beforeEach(() => {
        commit = jest.fn()
      })

      it('it should to reset the message', () => {
        UpgradeSubscription.actions.resetMessage({ commit })
        expect(commit).toHaveBeenCalled()
      })
    })
  })
})
