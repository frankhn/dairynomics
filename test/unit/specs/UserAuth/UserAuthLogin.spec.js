import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import AxiosMockAdapter from 'axios-mock-adapter'
import axios from 'axios'

import Vuelidate from 'vuelidate'
import UserAuthLogin from '@/components/ui/userAuth/UserAuthLogIn.vue'

const localVue = createLocalVue()

const mock = new AxiosMockAdapter(axios)

localVue.use(Vuex)
localVue.use(Vuelidate)

const watch = {
  loginForm: jest.fn()
}

const actions = {
  doLogin: jest.fn(),
  loadSendConfirmationForm: jest.fn()
}

// const data = { loginForm: {
//   phone: '+254',
//   password: '',
//   country_code: ''
// }}

const state = {
  userAuthLogin: {
    userId: null,
    loading: false,
    success: null,
    status: null
  }
}
const getters = {
  LoginDetails: () => state.userAuthLogin
}
const store = new Vuex.Store({
  getters, actions, watch
})

const wrapper = shallowMount(UserAuthLogin, {
  localVue,
  store
})

const response = {
  country_code: 'NG',
  country_name: 'Nigeria',
  city: 'Lagos',
  postal: null,
  latitude: 6.4531,
  longitude: 3.3958,
  IPv4: '41.215.245.118',
  state: 'Lagos'
}

mock.onGet('https://geoip-db.com/json/').reply(200, response)
describe('UserAuthLogin', () => {
  beforeEach(() => {
    mock.reset()

    // wrapper = shallow(UserAuthSignUp)
  })
  jest.mock('@/store')
  it('should render the component UserAuthLogin', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should submit form when the form is valid', () => {
    wrapper.setData({
      loginForm: {
        phone: 987654678,
        password: 1234,
        country_code: '+234'
      }
    })
    const submitSpy = jest.spyOn(wrapper.vm, 'loginSubmit')
    submitSpy()
    expect(actions.doLogin).toHaveBeenCalled()
  })

  it('should not password entry is invalid', () => {
    wrapper.setData({
      loginForm: {
        phone: 98767,
        password: 'joshua',
        country_code: 46788978
      }
    })
    actions.doLogin.mockReset()
    const submitSpy = jest.spyOn(wrapper.vm, 'loginSubmit')
    submitSpy()
    expect(actions.doLogin).not.toHaveBeenCalled()
  })

  it('lostpin function should call loadsendconfirmationForm', () => {
    const lostPinSpy = jest.spyOn(wrapper.vm, 'lostPin')
    lostPinSpy()
    expect(actions.loadSendConfirmationForm).toHaveBeenCalled()
  })
})
