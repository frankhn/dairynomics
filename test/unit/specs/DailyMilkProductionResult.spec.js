import { shallowMount } from '@vue/test-utils'
import DailyMilkProductionResult from '@/pages/DailyMilkPage/DailyMilkProductionResult.vue'
import SocialMediaShare from '@/components/layout/SocialMediaShare.vue'

jest.mock('@/mixins/isLoggedInMixin')
describe('@/pages/DailyMilkPage/DailyMilkCount.vue', () => {
  const wrapper = shallowMount(DailyMilkProductionResult, { propsData: {
    quantityOfMilk: 0
  }})
  it('does not render the socialMediaShare component if results category is in the poor category', () => {
    wrapper.setProps({ quantityOfMilk: 3 })
    expect(wrapper.contains(SocialMediaShare)).toBe(false)
  })
  it('renders the socialMediaShare component if results category is either good or average', () => {
    wrapper.setProps({ quantityOfMilk: 12 })
    expect(wrapper.contains(SocialMediaShare)).toBe(true)
    wrapper.setProps({ quantityOfMilk: 20 })
    expect(wrapper.contains(SocialMediaShare)).toBe(true)
  })
  it('renders the correct markup when quantity of milk is in the good category', () => {
    wrapper.setProps({ quantityOfMilk: 20 })
    expect(wrapper.html()).toContain('<h3 class="title">Excellent!</h3>')
    expect(wrapper.html()).toContain('<p class="info-message">You\'re doing pretty well but you can do better!</p>')
  })
  it('renders the correct markup when quantity of milk is in the poor category', () => {
    wrapper.setProps({ quantityOfMilk: 3 })
    expect(wrapper.html()).toContain('<h3 class="title">Below Average</h3>')
    expect(wrapper.html()).toContain('<p class="info-message">Your farm can perform so much better!</p>')
  })
  it('renders the correct markup when quantity of milk is in the average category', () => {
    wrapper.setProps({ quantityOfMilk: 11 })
    expect(wrapper.html()).toContain('<h3 class="title">Err.. Ok..</h3>')
    expect(wrapper.html()).toContain('<p class="info-message">You\'re doing fine, but you can always do better</p>')
    wrapper.setProps({ quantityOfMilk: 19 })
    expect(wrapper.html()).toContain('<h3 class="title">Err.. Ok..</h3>')
    expect(wrapper.html()).toContain('<p class="info-message">You\'re doing fine, but you can always do better</p>')
    wrapper.setProps({ quantityOfMilk: 13 })
    expect(wrapper.html()).toContain('<h3 class="title">Err.. Ok..</h3>')
    expect(wrapper.html()).toContain('<p class="info-message">You\'re doing fine, but you can always do better</p>')
  })
})
