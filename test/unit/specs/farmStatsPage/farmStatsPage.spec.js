import { shallowMount } from '@vue/test-utils'
import FarmStatsPage from '@/pages/FarmStatsPage/'
import CowsCount from '@/components/ui/CowsCount/'
import MilkMeasure from '@/pages/FarmStatsPage/MilkMeasure/'
import MilkPrice from '@/pages/FarmStatsPage/MilkPrice/'
import RevenueProjection from '@/pages/FarmStatsPage/RevenueProjection/'

describe('FarmStatsPage', () => {
  let wrapper

  beforeAll(() => {
    wrapper = shallowMount(FarmStatsPage)
  })

  it('renders without errors', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render "CowsCount" only on initial rendering', () => {
    expect(wrapper.findAll(CowsCount).length).toBe(1)
    expect(wrapper.findAll(MilkMeasure).length).toBe(0)
    expect(wrapper.findAll(MilkPrice).length).toBe(0)
    expect(wrapper.findAll(RevenueProjection).length).toBe(0)
  })

  it('should render "MilkMeasure" only', () => {
    wrapper.find(CowsCount).vm.$emit('submit', {numberOfCows: 10})
    expect(wrapper.findAll(CowsCount).length).toBe(0)
    expect(wrapper.findAll(MilkMeasure).length).toBe(1)
    expect(wrapper.findAll(MilkPrice).length).toBe(0)
    expect(wrapper.findAll(RevenueProjection).length).toBe(0)
  })

  it('should render "cowsCount" only', () => {
    wrapper.find(MilkMeasure).vm.$emit('changeStage')
    expect(wrapper.findAll(CowsCount).length).toBe(1)
    expect(wrapper.findAll(MilkMeasure).length).toBe(0)
    expect(wrapper.findAll(MilkPrice).length).toBe(0)
    expect(wrapper.findAll(RevenueProjection).length).toBe(0)
  })
})
