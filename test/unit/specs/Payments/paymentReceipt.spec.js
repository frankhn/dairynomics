import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import PaymentReceipt from '@/components/ui/paymentDetails/PaymentReceipt'

const localVue = createLocalVue()
localVue.use(Vuex)

const actions = {

}
const store = new Vuex.Store({
  actions
})

const receipt = {
  invoiceNumber: '#12345',
  amountToBePaid: 9000
}

const orderItems = [
  {
    'id': '2a0160f0-ca30-4f43-a153-d783a37381a2',
    'orderId': '9ddb64b2-b3a6-49be-a2d2-0f05d7872c6d',
    'userId': 1878,
    'productId': '351043bb-3460-4937-902c-e450ab2afc46',
    'quantity': 5,
    'amount': 5,
    'createdAt': '2019-08-21T19:15:00.193Z',
    'updatedAt': '2019-08-21T19:15:00.193Z',
    'product': {
      'categoryId': 58556,
      'productName': 'omnis at accusamus',
      'description': 'Rerum et nobis quia magnam quis eos culpa quibusdam consequatur.',
      'photos': []
    }
  }
]

const billing = {
  'address': 'Nairobi, Kenya',
  'fullName': 'Ryan Wire',
  'phoneNumber': '+254724374281'
}

const wrapper = shallowMount(PaymentReceipt, {
  propsData: {
    receipt,
    orderItems,
    billing
  },
  localVue,
  store
})
describe('PaymentReceipt', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render when phone number does not have a leading 0', () => {
    const preStore = new Vuex.Store({
      actions
    })
    const newWrapper = shallowMount(PaymentReceipt, {
      propsData: {
        receipt,
        orderItems,
        billing
      },
      localVue,
      store: preStore})
    expect(newWrapper.isVueInstance()).toBeTruthy()
  })

  it('Should call the dowloadReceipt method on clicking download button', () => {
    const downloadReceiptSpy = jest.spyOn(wrapper.vm, 'downloadReceipt')
    downloadReceiptSpy()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('close Modal should call emit closeModal', () => {
    const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal')
    closeModalSpy()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
