import { shallowMount } from '@vue/test-utils'
import PaymentModal from '@/components/ui/PaymentModal.vue'

describe('Payment Modal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(PaymentModal)

    wrapper.setData({
      content: 'receipt'
    })
  })

  it('close Modal should call emit closeModal', () => {
    const closeModalSpy = jest.spyOn(wrapper.vm, 'closeModal')
    closeModalSpy()
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
