import { shallowMount } from '@vue/test-utils'
import UserImage from '@/pages/Message/UserImage'

describe('UserImage', () => {
  let wrapper

  it('renders without error', () => {
    wrapper = shallowMount(UserImage)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('renders with props', () => {
    wrapper = shallowMount(UserImage, {
      propsData: {
        doReverse: true
      }
    })
    expect(wrapper.props().doReverse).toBeTruthy()
  })

  it('renders with the appropriate size if size is provided', () => {
    wrapper = shallowMount(UserImage, {
      propsData: {
        size: 'big'
      }
    })
    expect(wrapper.vm.getImageClass).toEqual('image big')
  })

  it('renders with the default size if size is not provided', () => {
    wrapper = shallowMount(UserImage, {
      propsData: {
        size: 'mini'
      }
    })
    expect(wrapper.vm.getImageClass).toEqual('image')
  })
})
