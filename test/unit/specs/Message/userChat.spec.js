import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserChat from '@/pages/Message/UserChat'
import UserImage from '@/pages/Message/UserImage'
import Vuex from 'vuex'

let getters
let actions
const localVue = createLocalVue()
localVue.use(Vuex)

const messagesMock = [{
  'id': 1094,
  'sender_id': 3554,
  'receiver_id': 3564,
  'message': 'Something else',
  'viewed': 0,
  'created_at': '08:48AM June 28, 2019',
  'updated_at': '08:48AM June 28, 2019',
  doReverse: false,
  status: true
}, {
  'id': 1094,
  'sender_id': 3564,
  'receiver_id': 3554,
  'message': 'Something else',
  'viewed': 0,
  'created_at': '08:48AM June 28, 2019',
  'updated_at': '08:48AM June 28, 2019',
  doReverse: false,
  status: true
}]

const mockUsers = [{
  'id': '3564',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}, {
  'id': '3554',
  'first_name': 'Kristoffer',
  'last_name': 'Stoltenberg',
  'email': 'kristofferstoltenberg1@testusers.cowsoko',
  'country': 'Uganda',
  'country_code': '+256',
  'phone': '0714433011',
  'account_type': '',
  'county': 'Boma',
  'profile': {
    'pic': 'https://beta.cowsoko.com/storage/img/avatar_dummy_person_2.jpg',
    'bio': null,
    'social': {
      'facebook': null,
      'twitter': null
    }
  },
  'verified_account': 1,
  'is_seller': 0,
  'is_expert': 0
}]

describe('UserChat', () => {
  let wrapper
  beforeAll(() => {
    getters = {
      chatMate: () => mockUsers[0],
      USER_PROFILE: () => mockUsers[1]
    }
    actions = {
      postMessageActions: jest.fn()
    }
    wrapper = shallowMount(UserChat, {
      propsData: {
        message: messagesMock[0],
        doReverse: false
      },
      localVue,
      store: new Vuex.Store({ actions, getters })
    })
  })

  it('renders without an error', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('renders with a child component', () => {
    expect(wrapper.find(UserImage).exists()).toBeTruthy()
  })

  it('sets the correct default values', () => {
    expect(wrapper.vm.doReverse).toBeFalsy()
  })

  it('renders with statusClasses computed prop', () => {
    expect(wrapper.vm.getStatusClasses).toBe('delivered')
  })

  it('renders with reverse computed prop', () => {
    wrapper.setProps({
      message: { ...messagesMock[0], doReverse: true }
    })
    expect(wrapper.vm.doReverse).toBeFalsy()
  })

  it('renders chat with the correct associated user profile photo', () => {
    wrapper.setProps({
      message: { ...messagesMock[0], doReverse: true }
    })
    expect(wrapper.vm.getPhotoURL).toEqual(mockUsers[0].profile.pic)
  })

  it('renders chat with the correct associated user profile photo', () => {
    wrapper.setProps({
      doReverse: false
    })
    expect(wrapper.vm.getUserChatClasses).toBeTruthy()
  })

  it('renders with the appropriate chat message div', () => {
    wrapper.setProps({
      message: { ...messagesMock[0], doReverse: true }
    })
    expect(wrapper.vm.getUserChatClasses).toEqual('chat sender')
  })

  it('renders a chat with not delivered error', () => {
    wrapper.setProps({
      message: { ...messagesMock[0], status: false }
    })
    expect(wrapper.vm.getStatusClasses).toBe('fail')
  })

  it('calls resendMessage', () => {
    const resendMessageSpy = jest.spyOn(wrapper.vm, 'resendMessage')
    resendMessageSpy()
    expect(wrapper.vm.resendMessage).toBeCalled()
  })
})
