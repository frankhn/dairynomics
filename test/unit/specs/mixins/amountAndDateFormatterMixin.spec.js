import { amountAndDateFormatterMixin } from '@/mixins/amountAndDateFormatterMixin'

describe('Should return formatted values for date and amount', () => {
  it('should return an amount formatted with commas', () => {
    const result = amountAndDateFormatterMixin.methods.getAmount(1000)
    expect(result).toEqual('KES 1,000')
  })
  it('should return zero when no amount is provided', () => {
    const result = amountAndDateFormatterMixin.methods.getAmount()
    expect(result).toEqual('KES 0')
  })
  it('should return a formatted date', () => {
    const result = amountAndDateFormatterMixin.methods.getDate('2019-07-01T06:51:46.357Z')
    expect(result).toEqual('Jul 1st, 2019')
  })
})
