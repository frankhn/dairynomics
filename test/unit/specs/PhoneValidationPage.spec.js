import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import PhoneValidation from '@/pages/PhoneValidation'

const localVue = createLocalVue()
localVue.use(Vuex)

const store = new Vuex.Store({
  state: {}
})
describe('Phone validation view page', () => {
  it('Test component', () => {
    const wrapper = shallowMount(PhoneValidation, store)
    expect(wrapper.find({name: 'BasePhoneValidation'}).exists()).toBe(true)
  })
})
