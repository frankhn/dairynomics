import { shallowMount } from '@vue/test-utils'
import DailyMilkPage from '@/pages/DailyMilkPage/index.vue'
import DailyMilkCount from '@/pages/DailyMilkPage/DailyMilkCount.vue'
import DailyMilkProductionResult from '@/pages/DailyMilkPage/DailyMilkProductionResult.vue'

describe('@/pages/DailyMilkPage/DailyMilkCount.vue', () => {
  const wrapper = shallowMount(DailyMilkPage)
  it('contains DailyMilkCount component if the staging section is DailyMilkCount', () => {
    wrapper.setData({ stagingSection: 'DailyMilkCount' })
    expect(wrapper.contains(DailyMilkCount)).toBe(true)
  })
  it('contains DailyMilkProductionResult component if the staging section is DailyMilkSection', () => {
    wrapper.setData({ stagingSection: 'DailyMilkSection' })
    expect(wrapper.contains(DailyMilkProductionResult)).toBe(true)
  })
  it('calls changeStagingSection method when @changeStagingSection happens', () => {
    wrapper.setData({ stagingSection: 'DailyMilkCount' })
    wrapper.find(DailyMilkCount).vm.$emit('changeStagingSection', 'DailyMilkSection')
    expect(wrapper.vm.stagingSection).toBe('DailyMilkSection')
  })
  it('calls updateMilkQuantity method when @quantityOfMilk happens', () => {
    wrapper.setData({ stagingSection: 'DailyMilkCount', quantityOfMilk: 3 })
    wrapper.find(DailyMilkCount).vm.$emit('quantityOfMilk', 20)
    expect(wrapper.vm.stagingSection).toBe('DailyMilkSection')
    expect(wrapper.vm.quantityOfMilk).toEqual(20)
  })
})
