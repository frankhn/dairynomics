import {createLocalVue, shallowMount} from '@vue/test-utils'
import Vuex from 'vuex'
import ProfileSideBar from '@/components/ui/ProfileSideBar.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

export const mockState = {
  owner: 'owner',
  location: 12,
  num_cows: 21,
  land_size: 21,
  cow_breeds: 23,
  milk_production: 12,
  photo: ['https://beta.cowsoko.com/storage/img/farm_farmplatea_1529567564_6df6f.jpg']

}

const getters = {
  USER_PROFILE: () => ({
    account_type: 'joshua',
    county: 'joshua',
    first_name: 'joshua',
    last_name: 'obasaju'
  }),
  PROFILE_PIC: () => 'https://joshua.jpg'
}

const preGetters = {
  USER_PROFILE: () => ({
    account_type: 'joshua',
    county: 'joshua',
    first_name: null,
    last_name: 'obasaju'
  }),
  PROFILE_PIC: () => 'https://joshua.jpg'
}

const actions = {
  GET_USER_PROFILE: jest.fn()

}
const store = new Vuex.Store({
  getters,
  actions
})
const wrapper = shallowMount(ProfileSideBar, {localVue, store})
describe('ProfileSideBar. vue test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('should render when first_name is undefined', () => {
    const preStore = new Vuex.Store({
      actions,
      getters: preGetters
    })
    const alteredWrapper = shallowMount(ProfileSideBar, {localVue, store: preStore})
    expect(alteredWrapper.vm.fullName).toEqual('')
  })
})
