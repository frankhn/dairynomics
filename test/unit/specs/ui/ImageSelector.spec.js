import { shallowMount } from '@vue/test-utils'
import ImageSelector from '@/components/ui/ImageSelector.vue'

describe('@components/ui/ImageSelector.vue', () => {
  const wrapper = shallowMount(ImageSelector)
  it('contains an input element', () => {
    expect(wrapper.contains('input')).toBe(true)
  })
  it('emits a "load" event when image is selected', () => {
    wrapper.find('input').trigger('change')
    expect(wrapper.emitted('load').length).toEqual(1)
  })
})
