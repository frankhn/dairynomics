import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Router from 'vue-router'
import EcommerceSideBar from '@/components/ui/EcommerceSideBar.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(Router)

Storage.prototype.getItem = jest.fn(
  () =>
    'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYmV0YS5jb3dzb2tvLmNvbVwvYXBpXC92MVwvbG9naW4iLCJpYXQiOjE1NjYyOTIxMjQsImV4cCI6MTU2NjM3ODUyNCwibmJmIjoxNTY2MjkyMTI0LCJqdGkiOiJ5cngxVmV5RGtlUzJZU0x2Iiwic3ViIjoxOTIzLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIiwiYWNjb3VudF90eXBlIjoiSSBhbSBhIHNlcnZpY2UgcHJvdmlkZXIifQ.1EQw4Z9ae8ZqfgfgUPX4sR3gkyCEFFLUgPVAYZ64S8A'
)

const getters = {
  USER_PROFILE: () => ({
    account_type: ''
  })
}

const actions = {
  GET_USER_PROFILE: jest.fn()
}
const store = new Vuex.Store({
  getters,
  actions
})

const wrapper = shallowMount(EcommerceSideBar, { localVue, store })

describe('Ecommerce side bar test', () => {
  it('should render Ecommerce side bar', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
