import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserAuthLogIn from '@/components/ui/userAuth/UserAuthLogIn.vue'
import Vuelidate from 'vuelidate'
import 'isomorphic-fetch'

const localVue = createLocalVue()
localVue.use(Vuelidate)
localVue.use(Vuex)
describe('UserAuthLogIn.vue', () => {
  let wrapper
  let actions
  let store
  let getters
  let data
  beforeAll(() => {
    actions = {
      doLogin: jest.fn()
    }
    data = {
      loginForm: {
        phone: '',
        password: '',
        country_code: ''
      }
    }
    getters = {
      LoginDetails: () => {
        return { loggingIn: false, loginError: null, loginSuccessful: false }
      }
    }
    store = new Vuex.Store({
      actions,
      getters,
      data
    })
  })
  it('should trigger and throw an error when form is invalid', () => {
    wrapper = shallowMount(UserAuthLogIn, { store, localVue })
    wrapper.setData({
      loginForm: {
        phone: '0754063403',
        password: '1234',
        country_code: '+256'
      }
    })
    wrapper.find('button[type="submit').trigger('click')
    wrapper.find('form').trigger('submit.prevent')
  })
  it('should render the component ', () => {
    wrapper = shallowMount(UserAuthLogIn, { store, localVue })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
