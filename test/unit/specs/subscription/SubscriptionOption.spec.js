import { shallowMount, createLocalVue } from '@vue/test-utils'
import SubscriptionOptions from '@/pages/Subscriptionoption/SubscriptionOptions'
// import Vuex from 'vuex'
import Router from 'vue-router'

jest.mock('@/router/router')
const localVue = createLocalVue()
// localVue.use(Vuex)
localVue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [{
    path: '/'
  }]
})

const wrapper = shallowMount(SubscriptionOptions, {
  propsData: {
    first: 500,
    second: 800,
    third: 1000
  },
  localVue,
  router
})

wrapper.vm.$router.push = jest.fn()

describe('testing subscription-option page', () => {
  it('testing if subscription-option is rendered successfully', () => {
    expect(wrapper.contains('div')).toBe(true)
  })

  it('testing selectMembership method', () => {
    const selectMembershipSpy = jest.spyOn(wrapper.vm, 'selectMembership')
    selectMembershipSpy(3, 500)
    expect(wrapper.vm.$router.push).toHaveBeenCalled()
  })
})
