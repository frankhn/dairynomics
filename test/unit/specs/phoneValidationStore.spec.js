import axios from 'axios'
import phoneValidation from '@/store/modules/phoneValidation'
import store from '@/store'
import MockAdapter from 'axios-mock-adapter'
import { mock } from '@/store/modules/mocks/mockInstance'

jest.mock('@/router/router')

describe('Phone Validation mutations', () => {
  it('test GENERATED_TOKEN mutation', () => {
    let code = 1234
    const state = {}
    let verification = {
      country_code: '+256',
      phone: '0778072363'
    }
    phoneValidation.mutations.GENERATED_TOKEN(state, verification, code)
    expect(state.verificationCode).toEqual(1234)
  })
  it('test VALID_TOKEN mutation', () => {
    const token = 1001
    const state = {}
    phoneValidation.mutations.VALID_TOKEN(state, token)
    expect(state.token).toEqual(1001)
    expect(JSON.stringify(store.state)).toContain('phoneValidation')
  })
  it('test INVALID_ERROR mutation', () => {
    const error = 'This is the error'
    const state = {}
    phoneValidation.mutations.INVALID_ERROR(state, error)
    expect(state.erro).toEqual(error)
  })
  it('test SPIN_ICON mutation', () => {
    const status = true
    const state = {}
    phoneValidation.mutations.SPIN_ICON(state, status)
    expect(state.spinner).toEqual(status)
  })
  it('test REFRESH_ERROR mutation', () => {
    const state = {}
    phoneValidation.mutations.REFRESH_ERROR(state)
    expect(state.erro).toEqual(null)
  })
  it('test SEND_MESSAGE mutation', () => {
    const state = {}
    const message = 'SMS TEXT'
    phoneValidation.mutations.SEND_MESSAGE(state, message)
    expect(state.message).toEqual(message)
  })
  it('test GETTERS', () => {
    const state = {randomState: 'state'}
    phoneValidation.getters.getterData(state)
    expect(state).toEqual(state)
  })
})

describe('Phone Validation actions', () => {
  afterEach(() => {
    mock.reset()
  })
  it('test request for PIN number with Phone number and country code as the body', () => {
    const commit = jest.fn()
    const mocked = new MockAdapter(axios)
    const contactDetails = {country_code: '+564', phone: '0000000000'}
    mocked.onPost('https://beta.cowsoko.com/api/v1/send_verification_code').reply(201, contactDetails)
    phoneValidation.actions.getRandomToken({commit}, contactDetails).then(() => {
      expect(mocked.history.post.length).toBe(1)
    })
  })
  it('test error is returned if Phone number or country code is invalid', () => {
    const commit = jest.fn()
    const resp = {country_code: '+25', phone: '07780723'}
    const mocked = new MockAdapter(axios)
    const contactDetails = {country_code: '+564', phone: '0000000000'}
    mocked.onPost('https://beta.cowsoko.com/api/v1/send_verification_code').reply(422, resp)
    phoneValidation.actions.getRandomToken({commit}, contactDetails).then((resp) => {
      expect(resp.toString()).toBe('Error: Request failed with status code 422')
    })
  })
  it('test verifyToken success', async () => {
    const commit = jest.fn()
    const rootState = {user_id: '1883', signup: {userAuthSignUp: {userId: '1883'}}}
    const token = '5554'
    const mocked = new MockAdapter(axios)
    const resp = {country_code: '+256', phone: '0778072363', success: true}
    mocked.onPut('https://beta.cowsoko.com/api/v1/verify_code/1883').reply(201, resp)
    await phoneValidation.actions.verifyToken({commit, rootState}, token).then(() => {
      expect(mocked.history.put[0].data).toBe(JSON.stringify({verification_code: '5554'}))
    })
  })
  it('test verifyToken failure', () => {
    const commit = jest.fn()
    const rootState = {verification: {verification_code: '7889'}, signup: {userAuthSignUp: {userId: '1883'}}}
    const token = '5554'
    const mocked = new MockAdapter(axios)
    const resp = {country_code: '+256', phone: '0778072363'}
    mocked.onPut('https://beta.cowsoko.com/api/v1/verify_code/1883').reply(422, resp)
    phoneValidation.actions.verifyToken({commit, rootState}, token).then(() => {
      expect(mocked.history.put.length).toBe(1)
    })
  })
  it('test sendMessage success', () => {
    const commit = jest.fn()
    const mocked = new MockAdapter(axios)
    const rootState = {verification: {verification_code: '7889'}, signup: {userAuthSignUp: {userId: '1883'}}}
    const state = {verification: {verification_code: '7889'}}
    mocked.onPost('https://beta.cowsoko.com/api/v1/send_sms/1883').reply(201, {message: {}})
    phoneValidation.actions.sendMessage({commit, rootState, state}).then(resp => {
      expect(mocked.history.post[0].data).toContain('Thank you for joining Dairynomics')
    })
  })
  it('test sendMessage failure', () => {
    const commit = jest.fn()
    const mocked = new MockAdapter(axios)
    const rootState = {verification: {verification_code: '7889'}, signup: {userAuthSignUp: {userId: '0000'}}}
    const state = {verification: {verification_code: '7889'}}
    mocked.onPost('https://beta.cowsoko.com/api/v1/send_sms/0000').reply(422, {message: {}})
    phoneValidation.actions.sendMessage({commit, rootState, state}).then(resp => {
      expect(mocked.history.post.length).toBe(1)
    })
  })
})
