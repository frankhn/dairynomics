const user = {
  id: '121212',
  first_name: 'John',
  last_name: 'Doe',
  email: null,
  country: 'DoeLand',
  country_code: '+234',
  phone: '123456789',
  account_type: 'I am a dairy farmer',
  county: 'Doe County',
  profile: {
    pic: 'pic.jpg',
    bio: null,
    social: {
      facebook: null,
      twitter: null
    }
  },
  verified_account: 1
}

export default user
