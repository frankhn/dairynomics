import { mount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
import ServiceProviderUpdate from '@/pages/ServiceProviderUpdates/ServiceProviderUpdate'
import ImageSelector from '@/components/ui/ImageSelector'
import expert from '@/store/modules/expert'

const localVue = createLocalVue()

localVue.use(Vuex)

const mock = new MockAdapter(axios)

jest.mock('@/store')
jest.mock('@/router/router')

describe('ServiceProviderUpdatePage', () => {
  let store
  let actions
  let wrapper

  beforeEach(() => {
    actions = {
      POST_EXPERT_UPDATE: jest.fn()
    }

    const propObject = () => ({
      options: [{
        id: 2,
        topic: 'Cow herding'
      }],
      token: '2wudwuuwu388598gjvdklsl',
      expertId: 210,
      blogs: []
    })

    store = new Vuex.Store({
      modules: {
        expert: {
          state: expert.state,
          getters: expert.getters,
          actions,
          mutations: expert.mutations
        }
      }
    })
    wrapper = mount(ServiceProviderUpdate, { store, localVue, propsData: propObject() })
  })

  it('should dispatch action when submit button is clicked and all the fields are filled', () => {
    wrapper.setData({message: 'Test message', selectFieldValue: 1})
    wrapper.vm.handleSubmit()
    expect(actions.POST_EXPERT_UPDATE).toHaveBeenCalled()
  })
  it('COMMITS a CREATE_EXPERT_UPDATE and two TOGGLE_LOADING mutations when submit button is clicked'
    , (done) => {
      wrapper.vm.handleSubmit()

      const context = {
        commit: jest.fn()
      }

      const payload = {
        data: {
          photo: '',
          user_id: 20,
          topic: 5,
          body: 'This is the payload'
        },
        token: '2wudwuuwu388598gjvdklsl'
      }

      mock.onPost('https://beta.cowsoko.com/api/v1/updates').reply(201, {
        data: {
          data: { experts: [
            { id: 1, topic: 'Cow Health and Maternity', body: 'Test body' }
          ]
          }
        }
      })

      expert.actions.POST_EXPERT_UPDATE(context, payload).then(() => {
        expect(context.commit).toHaveBeenCalledTimes(3)
        done()
      })
    })
  it('does not COMMIT a CREATE_EXPERT_UPDATE mutation if there\'s an error', (done) => {
    const context = {
      commit: {
        POST_EXPERT_UPDATE: jest.fn()
      }
    }

    const payload = {
      data: {
        photo: '',
        user_id: 20,
        topic: 5,
        body: 'This is the payload'
      },
      token: '2wudwuuwu388598gjvdklsl'
    }

    mock.onPost('https://beta.cowsoko.com/api/v1/updates').networkError()

    expert.actions.POST_EXPERT_UPDATE(context, payload).catch(() => {
      expect(context.commit.POST_EXPERT_UPDATE).toHaveBeenCalledTimes(0)
      done()
    })
  })
  it('triggers the handleImagesUpload method when @load is emitted from the ImageSelector', () => {
    const mock = jest.fn()
    wrapper.setMethods({ handleImagesUpload: mock })
    const fileList = {
      name: 'Screen Shot 2019-04-10 at 1.33.34 PM.png',
      lastModified: 1554899615052,
      size: 685324
    }
    wrapper.find(ImageSelector).vm.$emit('load', fileList)
    expect(mock).toHaveBeenCalled()
    expect(mock).toHaveBeenCalledWith(fileList)
  })
  it('changes the selectFieldValue when the handleSelectFieldChange method is triggered', () => {
    wrapper.setData({ selectFieldValue: 2 })
    wrapper.vm.handleSelectFieldChange({
      id: 1,
      topic: 'building for a cow'
    })
    expect(wrapper.vm.selectFieldValue).toEqual(1)
  })
  it('changes the imagesUploaded value when the handleImagesUpload method is triggered', () => {
    wrapper.setData({ imagesUploaded: [{
      name: 'Screen Shot 2019-04-10 at 1.33.34 PM.png',
      lastModified: 1554899615052,
      size: 685324
    }]
    })
    const fileList = {name: 'Screen Shot 2019-04-10 at 1.33.34 PM.png', lastModified: 1554899615052, size: 685324}
    wrapper.vm.handleImagesUpload(fileList)
    expect(wrapper.vm.imagesUploaded).toEqual([{name: 'Screen Shot 2019-04-10 at 1.33.34 PM.png', lastModified: 1554899615052, size: 685324}])
  })
})
