import { shallowMount, createLocalVue } from '@vue/test-utils'
import UploadProduct from '@/pages/InputSellerActivity/UploadProduct.vue'
import Vuex from 'vuex'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import vuelidate from 'vuelidate'
const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(FontAwesomeIcon)
localVue.use(vuelidate)
const store = new Vuex.Store({
  state: {
    UserProfileModule: {
      user: {
        inputs_offered: '["hello"]'
      }
    },
    uploadInputStore: {
      selectedInputs: []
    },
    uploadProductStore: {
      loading: false,
      error: {},
      message: '',
      userData: {},
      currentComponent: 'ProductSection',
      products: []
    }
  },
  actions: {
    fetchProducts: jest.fn(),
    submitForm: jest.fn()
  }
})

const wrapper = shallowMount(UploadProduct, {
  localVue,
  store
})

describe('UploadProduct Component', () => {
  it('should render the Upload Product Component properly', () => {
    expect(wrapper.isVueInstance).toBeTruthy()
  })

  it('should call watch method on AvailableOptions Selected', () => {
    wrapper.setData({
      productInfo: {
        name: 'tractor',
        description: 'a big tractor',
        price: '1200',
        product_type: 'Dairymeal',
        quantity: '2',
        photo: []
      },
      availableOptionsSelected: 'Available',
      availableOptions: ['Unavailable', 'Available']
    })
    wrapper.vm.$options.watch.availableOptionsSelected.call(
      wrapper.vm,
      'cellfam'
    )
    expect(wrapper.vm.productInfo.availability).toEqual(1)
  })

  it('should call the handleSubmit method', () => {
    const handleSubmit = jest.spyOn(wrapper.vm, 'handleSubmit')
    handleSubmit()
    expect(wrapper.vm.handleSubmit).toBeCalled()
  })

  it('should assert that SubmitStatus is Error', () => {
    const submitForm = jest.spyOn(wrapper.vm, 'submitForm')
    wrapper.setData({
      productInfo: {
        name: 'tractor',
        description: 'a big tractor',
        price: '1200',
        product_type: 'Dairymeal',
        quantity: '2',
        photo: [],
        availability: null
      },
      availableOptionsSelected: 'Available',
      availableOptions: ['Unavailable', 'Available']
    })
    submitForm()
    expect(wrapper.vm.submitForm).toBeCalled()
  })

  it('should call the submitForm method when the form is valid', () => {
    const submitForm = jest.spyOn(wrapper.vm, 'submitForm')
    wrapper.setData({
      productInfo: {
        name: 'tractor',
        description: 'a big tractor',
        price: 1200,
        product_type: 'Dairymeal',
        quantity: 2,
        photo: ['https://joshua.jpg'],
        availability: 1
      },
      availableOptionsSelected: 'Available',
      availableOptions: ['Unavailable', 'Available']
    })
    submitForm()
    expect(wrapper.vm.submitForm).toBeCalled()
  })

  it('should call the dragEnter method', () => {
    const dragEnter = jest.spyOn(wrapper.vm, 'dragEnter')
    dragEnter()
    expect(wrapper.vm.dragEnter).toBeCalled()
  })

  it('should call the dragLeave method', () => {
    const dragLeave = jest.spyOn(wrapper.vm, 'dragLeave')
    dragLeave()
    expect(wrapper.vm.dragLeave).toBeCalled()
  })

  it('should call the RemoveImg method', () => {
    const removeImg = jest.spyOn(wrapper.vm, 'removeImg')
    removeImg()
    expect(wrapper.vm.removeImg).toBeCalled()
  })
})
