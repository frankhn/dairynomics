import { shallowMount, createLocalVue } from '@vue/test-utils'
import IndividualItemInfo from '@/pages/IndividualItem/IndividualItemInfo.vue'

jest.mock('@/store')

const localVue = createLocalVue()

describe('IndividualItemInfo', () => {
  let wrapper

  it('should render the IndividualItemInfo component', () => {
    wrapper = shallowMount(IndividualItemInfo, { localVue,
      propsData: {
        farmInfo: {
          images: 'image'
        }
      } })
    wrapper.setData({ buttonDetails: '<i class="fas fa-envelope"></i> <span icon="envelope">Contact expert</span>' })
    expect(wrapper.vm.buttonDetails).toEqual('<i class="fas fa-envelope"></i> <span icon="envelope">Contact expert</span>')
  })
})
