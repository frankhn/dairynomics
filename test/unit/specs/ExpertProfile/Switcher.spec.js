import { shallowMount, createLocalVue } from '@vue/test-utils'
import UserProfileSwitcher from '@/pages/ExpertProfilePage/Switcher.vue'
import Vuex from 'vuex'
import Router from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)
describe('UserProfileTimelinePage', () => {
  const $route = {
    name: 'someName'
  }
  const $router = {
    push: jest.fn()
  }

  let getters
  let store
  let wrapper
  const router = new Router({push: {name: 'someName'}})
  getters = {
    getExpertProfileInformation: () => {
      return { getExpertProfile: {profile: {pic: ''}},
        ExpertProfileError: null}
    }}
  store = new Vuex.Store({getters})
  wrapper = shallowMount(UserProfileSwitcher, { store,
    localVue,
    router,
    mocks: {
      $route,
      $router
    }
  })

  it('changes activeStatusLink when switcher is clicked', () => {
    wrapper.find('.my-activities').trigger('click')
    expect(wrapper.vm.isMyExpertProfileLinkActive).toEqual(true)
  })
})
