import {createLocalVue, shallowMount} from '@vue/test-utils'
import Vuex from 'vuex'
import ExpertUpdatePage from '@/pages/ExpertProfilePage/ExpertUpdates.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

jest.mock('@/router/router')

const actions = {
  fetchUpdates: jest.fn()
}

const getters = {
  getExpertsUpdates: () => ([{
    user: {
      number_of_updates: 1
    }
  }]),
  getExpertFarmsError: () => false,
  getUpdatesRequestStatuses: () => ({
    isFetching: false
  })
}

const store = new Vuex.Store({
  actions,
  getters
})

const $route = {
  params: {
    id: 23
  }
}

const wrapper = shallowMount(ExpertUpdatePage, {localVue,
  store,
  mocks: {
    $route
  }})

const methodSpy = (method) => jest.spyOn(wrapper.vm, method)

describe('ExpertUpdates Test', () => {
  it('should render', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  it('loadMore should call this.fetchUpdates', () => {
    const loadMoreSpy = methodSpy('loadMore')
    loadMoreSpy()
    expect(actions.fetchUpdates).toHaveBeenCalled()
  })

  it('changeProfileSection should set this.activeStatusLink to isMyFarmLinkActive', () => {
    const changeProfileSpy = methodSpy('changeProfileSection')
    changeProfileSpy()
    expect(wrapper.vm.activeStatusLink).toEqual('isMyFarmLinkActive')
  })

  it('showLoading should return true and hideloadbutton to be false', () => {
    const expectedResult = wrapper.vm.showLoading
    expect(expectedResult).toBeTruthy()
    expect(wrapper.vm.hideLoadButton).toBeTruthy()
  })

  it('loadMoreButtonContent should return font-awesome icon', () => {
    const expectedResult = wrapper.vm.loadMoreButtonContent
    expect(expectedResult).toEqual('LOAD MORE')
  })

  it('showLoading should return true when this.getExpertsUpdates.length is zero', () => {
    const newGetters = {...getters,
      getExpertsUpdates: () => [],
      getUpdatesRequestStatuses: () => ({
        isFetching: true
      })
    }
    const newStore = new Vuex.Store({
      actions,
      getters: newGetters
    })

    const newWrapper = shallowMount(ExpertUpdatePage, {localVue,
      store: newStore,
      mocks: {
        $route
      }})
    const expectedResult = newWrapper.vm.showLoading
    expect(expectedResult).toBeTruthy()
    expect(newWrapper.vm.loadMoreButtonContent).toEqual('<i class="fas fa-spinner fa-spin"/>')
  })
})
