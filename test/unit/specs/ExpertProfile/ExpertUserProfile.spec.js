import Vuex from 'vuex'
import { shallowMount, createLocalVue } from '@vue/test-utils'
import ExpertUserProfile from '@/pages/ExpertProfilePage/ExpertUserProfile.vue'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('ExpertUserProfile.vue', () => {
  let actions
  let store
  let getters
  let wrapper
  const router = new VueRouter({params: {id: 1}})
  getters = {
    getExpertProfileInformation: () => {
      return { getExpertProfile: {profile: {pic: ''}},
        ExpertProfileError: null}
    }}
  actions = {
    expertProfile: jest.fn()
  }
  store = new Vuex.Store({
    actions, getters
  })

  wrapper = shallowMount(ExpertUserProfile, { store,
    localVue,
    router
  })
  it('should dispatch action when created', () => {
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})
