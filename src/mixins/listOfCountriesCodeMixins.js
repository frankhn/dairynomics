const flagPath = '/static/svg/flags'

export default {
  computed: {
    countryCodes () {
      const countryCodes = [
        {
          'name': 'Afghanistan',
          'code': '+93',
          'country_code': '+93',
          'name_code': 'Afghanistan: +93',
          'flag': `${flagPath}/af.svg`,
          'country_code_2': 'AF'
        },
        {
          'name': 'Albania',
          'code': '+355',
          'country_code': '+355',
          'name_code': 'Albania: +355',
          'flag': `${flagPath}/al.svg`,
          'country_code_2': 'AL'
        },
        {
          'name': 'Algeria',
          'code': '+213',
          'country_code': '+213',
          'name_code': 'Algeria: +213',
          'flag': `${flagPath}/dz.svg`,
          'country_code_2': 'DZ'
        },
        {
          'name': 'AmericanSamoa',
          'code': '+1684',
          'country_code': '+1684',
          'name_code': 'AmericanSamoa: +1684',
          'flag': `${flagPath}/as.svg`,
          'country_code_2': 'AS'
        },
        {
          'name': 'Andorra',
          'code': '+376',
          'country_code': '+376',
          'name_code': 'Andorra: +376',
          'flag': `${flagPath}/ad.svg`,
          'country_code_2': 'AD'
        },
        {
          'name': 'Angola',
          'code': '+244',
          'country_code': '+244',
          'name_code': 'Angola: +244',
          'flag': `${flagPath}/ao.svg`,
          'country_code_2': 'AO'
        },
        {
          'name': 'Anguilla',
          'code': '+1264',
          'country_code': '+1264',
          'name_code': 'Anguilla: +1264',
          'flag': `${flagPath}/ai.svg`,
          'country_code_2': 'AI'
        },
        {
          'name': 'Antarctica',
          'code': '+672',
          'country_code': '+672',
          'name_code': 'Antarctica: +672',
          'flag': `${flagPath}/aq.svg`,
          'country_code_2': 'AQ'
        },
        {
          'name': 'Antigua and Barbuda',
          'code': '+1268',
          'country_code': '+1268',
          'name_code': 'Antigua and Barbuda: +1268',
          'flag': `${flagPath}/ag.svg`,
          'country_code_2': 'AG'
        },
        {
          'name': 'Argentina',
          'code': '+54',
          'country_code': '+54',
          'name_code': 'Argentina: +54',
          'flag': `${flagPath}/ar.svg`,
          'country_code_2': 'AR'
        },
        {
          'name': 'Armenia',
          'code': '+374',
          'country_code': '+374',
          'name_code': 'Armenia: +374',
          'flag': `${flagPath}/am.svg`,
          'country_code_2': 'AM'
        },
        {
          'name': 'Aruba',
          'code': '+297',
          'country_code': '+297',
          'name_code': 'Aruba: +297',
          'flag': `${flagPath}/aw.svg`,
          'country_code_2': 'AW'
        },
        {
          'name': 'Australia',
          'code': '+61',
          'country_code': '+61',
          'name_code': 'Australia: +61',
          'flag': `${flagPath}/au.svg`,
          'country_code_2': 'AU'
        },
        {
          'name': 'Austria',
          'code': '+43',
          'country_code': '+43',
          'name_code': 'Austria: +43',
          'flag': `${flagPath}/at.svg`,
          'country_code_2': 'AT'
        },
        {
          'name': 'Azerbaijan',
          'code': '+994',
          'country_code': '+994',
          'name_code': 'Azerbaijan: +994',
          'flag': `${flagPath}/az.svg`,
          'country_code_2': 'AZ'
        },
        {
          'name': 'Bahamas',
          'code': '+1242',
          'country_code': '+1242',
          'name_code': 'Bahamas: +1242',
          'flag': `${flagPath}/bs.svg`,
          'country_code_2': 'BS'
        },
        {
          'name': 'Bahrain',
          'code': '+973',
          'country_code': '+973',
          'name_code': 'Bahrain: +973',
          'flag': `${flagPath}/bh.svg`,
          'country_code_2': 'BH'
        },
        {
          'name': 'Bangladesh',
          'code': '+880',
          'country_code': '+880',
          'name_code': 'Bangladesh: +880',
          'flag': `${flagPath}/bd.svg`,
          'country_code_2': 'BD'
        },
        {
          'name': 'Barbados',
          'code': '+1246',
          'country_code': '+1246',
          'name_code': 'Barbados: +1246',
          'flag': `${flagPath}/bb.svg`,
          'country_code_2': 'BB'
        },
        {
          'name': 'Belarus',
          'code': '+375',
          'country_code': '+375',
          'name_code': 'Belarus: +375',
          'flag': `${flagPath}/by.svg`,
          'country_code_2': 'BY'
        },
        {
          'name': 'Belgium',
          'code': '+32',
          'country_code': '+32',
          'name_code': 'Belgium: +32',
          'flag': `${flagPath}/be.svg`,
          'country_code_2': 'BE'
        },
        {
          'name': 'Belize',
          'code': '+501',
          'country_code': '+501',
          'name_code': 'Belize: +501',
          'flag': `${flagPath}/bz.svg`,
          'country_code_2': 'BZ'
        },
        {
          'name': 'Benin',
          'code': '+229',
          'country_code': '+229',
          'name_code': 'Benin: +229',
          'flag': `${flagPath}/bj.svg`,
          'country_code_2': 'BJ'
        },
        {
          'name': 'Bermuda',
          'code': '+1441',
          'country_code': '+1441',
          'name_code': 'Bermuda: +1441',
          'flag': `${flagPath}/bm.svg`,
          'country_code_2': 'BM'
        },
        {
          'name': 'Bhutan',
          'code': '+975',
          'country_code': '+975',
          'name_code': 'Bhutan: +975',
          'flag': `${flagPath}/bt.svg`,
          'country_code_2': 'BT'
        },
        {
          'name': 'Bolivia, Plurinational State of',
          'code': '+591',
          'country_code': '+591',
          'name_code': 'Bolivia, Plurinational State of: +591',
          'flag': `${flagPath}/bo.svg`,
          'country_code_2': 'BO'
        },
        {
          'name': 'Bosnia and Herzegovina',
          'code': '+387',
          'country_code': '+387',
          'name_code': 'Bosnia and Herzegovina: +387',
          'flag': `${flagPath}/ba.svg`,
          'country_code_2': 'BA'
        },
        {
          'name': 'Botswana',
          'code': '+267',
          'country_code': '+267',
          'name_code': 'Botswana: +267',
          'flag': `${flagPath}/bw.svg`,
          'country_code_2': 'BW'
        },
        {
          'name': 'Brazil',
          'code': '+55',
          'country_code': '+55',
          'name_code': 'Brazil: +55',
          'flag': `${flagPath}/br.svg`,
          'country_code_2': 'BR'
        },
        {
          'name': 'British Indian Ocean Territory',
          'code': '+246',
          'country_code': '+246',
          'name_code': 'British Indian Ocean Territory: +246',
          'flag': `${flagPath}/io.svg`,
          'country_code_2': 'IO'
        },
        {
          'name': 'Brunei Darussalam',
          'code': '+673',
          'country_code': '+673',
          'name_code': 'Brunei Darussalam: +673',
          'flag': `${flagPath}/bn.svg`,
          'country_code_2': 'BN'
        },
        {
          'name': 'Bulgaria',
          'code': '+359',
          'country_code': '+359',
          'name_code': 'Bulgaria: +359',
          'flag': `${flagPath}/bg.svg`,
          'country_code_2': 'BG'
        },
        {
          'name': 'Burkina Faso',
          'code': '+226',
          'country_code': '+226',
          'name_code': 'Burkina Faso: +226',
          'flag': `${flagPath}/bf.svg`,
          'country_code_2': 'BF'
        },
        {
          'name': 'Burundi',
          'code': '+257',
          'country_code': '+257',
          'name_code': 'Burundi: +257',
          'flag': `${flagPath}/bi.svg`,
          'country_code_2': 'BI'
        },
        {
          'name': 'Cambodia',
          'code': '+855',
          'country_code': '+855',
          'name_code': 'Cambodia: +855',
          'flag': `${flagPath}/kh.svg`,
          'country_code_2': 'KH'
        },
        {
          'name': 'Cameroon',
          'code': '+237',
          'country_code': '+237',
          'name_code': 'Cameroon: +237',
          'flag': `${flagPath}/cm.svg`,
          'country_code_2': 'CM'
        },
        {
          'name': 'Canada',
          'code': '+1',
          'country_code': '+1',
          'name_code': 'Canada: +1',
          'flag': `${flagPath}/ca.svg`,
          'country_code_2': 'CA'
        },
        {
          'name': 'Cape Verde',
          'code': '+238',
          'country_code': '+238',
          'name_code': 'Cape Verde: +238',
          'flag': `${flagPath}/cv.svg`,
          'country_code_2': 'CV'
        },
        {
          'name': 'Cayman Islands',
          'code': '+345',
          'country_code': '+345',
          'name_code': 'Cayman Islands: +345',
          'flag': `${flagPath}/ky.svg`,
          'country_code_2': 'KY'
        },
        {
          'name': 'Central African Republic',
          'code': '+236',
          'country_code': '+236',
          'name_code': 'Central African Republic: +236',
          'flag': `${flagPath}/cf.svg`,
          'country_code_2': 'CF'
        },
        {
          'name': 'Chad',
          'code': '+235',
          'country_code': '+235',
          'name_code': 'Chad: +235',
          'flag': `${flagPath}/td.svg`,
          'country_code_2': 'TD'
        },
        {
          'name': 'Chile',
          'code': '+56',
          'country_code': '+56',
          'name_code': 'Chile: +56',
          'flag': `${flagPath}/cl.svg`,
          'country_code_2': 'CL'
        },
        {
          'name': 'China',
          'code': '+86',
          'country_code': '+86',
          'name_code': 'China: +86',
          'flag': `${flagPath}/cn.svg`,
          'country_code_2': 'CN'
        },
        {
          'name': 'Christmas Island',
          'code': '+61',
          'country_code': '+61',
          'name_code': 'Christmas Island: +61',
          'flag': `${flagPath}/cx.svg`,
          'country_code_2': 'CX'
        },
        {
          'name': 'Cocos (Keeling) Islands',
          'code': '+61',
          'country_code': '+61',
          'name_code': 'Cocos (Keeling) Islands: +61',
          'flag': `${flagPath}/cc.svg`,
          'country_code_2': 'CC'
        },
        {
          'name': 'Colombia',
          'code': '+57',
          'country_code': '+57',
          'name_code': 'Colombia: +57',
          'flag': `${flagPath}/co.svg`,
          'country_code_2': 'CO'
        },
        {
          'name': 'Comoros',
          'code': '+269',
          'country_code': '+269',
          'name_code': 'Comoros: +269',
          'flag': `${flagPath}/km.svg`,
          'country_code_2': 'KM'
        },
        {
          'name': 'Congo',
          'code': '+242',
          'country_code': '+242',
          'name_code': 'Congo: +242',
          'flag': `${flagPath}/cg.svg`,
          'country_code_2': 'CG'
        },
        {
          'name': 'Congo, The Democratic Republic of the',
          'code': '+243',
          'country_code': '+243',
          'name_code': 'Congo, The Democratic Republic of the: +243',
          'flag': `${flagPath}/cd.svg`,
          'country_code_2': 'CD'
        },
        {
          'name': 'Cook Islands',
          'code': '+682',
          'country_code': '+682',
          'name_code': 'Cook Islands: +682',
          'flag': `${flagPath}/ck.svg`,
          'country_code_2': 'CK'
        },
        {
          'name': 'Costa Rica',
          'code': '+506',
          'country_code': '+506',
          'name_code': 'Costa Rica: +506',
          'flag': `${flagPath}/cr.svg`,
          'country_code_2': 'CR'
        },
        {
          'name': "Cote d'Ivoire",
          'code': '+225',
          'country_code': '+225',
          'name_code': "Cote d'Ivoire: +225",
          'flag': `${flagPath}/ci.svg`,
          'country_code_2': 'CI'
        },
        {
          'name': 'Croatia',
          'code': '+385',
          'country_code': '+385',
          'name_code': 'Croatia: +385',
          'flag': `${flagPath}/hr.svg`,
          'country_code_2': 'HR'
        },
        {
          'name': 'Cuba',
          'code': '+53',
          'country_code': '+53',
          'name_code': 'Cuba: +53',
          'flag': `${flagPath}/cu.svg`,
          'country_code_2': 'CU'
        },
        {
          'name': 'Cyprus',
          'code': '+537',
          'country_code': '+537',
          'name_code': 'Cyprus: +537',
          'flag': `${flagPath}/cy.svg`,
          'country_code_2': 'CY'
        },
        {
          'name': 'Czech Republic',
          'code': '+420',
          'country_code': '+420',
          'name_code': 'Czech Republic: +420',
          'flag': `${flagPath}/cz.svg`,
          'country_code_2': 'CZ'
        },
        {
          'name': 'Denmark',
          'code': '+45',
          'country_code': '+45',
          'name_code': 'Denmark: +45',
          'flag': `${flagPath}/dk.svg`,
          'country_code_2': 'DK'
        },
        {
          'name': 'Djibouti',
          'code': '+253',
          'country_code': '+253',
          'name_code': 'Djibouti: +253',
          'flag': `${flagPath}/dj.svg`,
          'country_code_2': 'DJ'
        },
        {
          'name': 'Dominica',
          'code': '+1767',
          'country_code': '+1767',
          'name_code': 'Dominica: +1767',
          'flag': `${flagPath}/dm.svg`,
          'country_code_2': 'DM'
        },
        {
          'name': 'Dominican Republic',
          'code': '+1849',
          'country_code': '+1849',
          'name_code': 'Dominican Republic: +1849',
          'flag': `${flagPath}/do.svg`,
          'country_code_2': 'DO'
        },
        {
          'name': 'Ecuador',
          'code': '+593',
          'country_code': '+593',
          'name_code': 'Ecuador: +593',
          'flag': `${flagPath}/ec.svg`,
          'country_code_2': 'EC'
        },
        {
          'name': 'Egypt',
          'code': '+20',
          'country_code': '+20',
          'name_code': 'Egypt: +20',
          'flag': `${flagPath}/eg.svg`,
          'country_code_2': 'EG'
        },
        {
          'name': 'El Salvador',
          'code': '+503',
          'country_code': '+503',
          'name_code': 'El Salvador: +503',
          'flag': `${flagPath}/sv.svg`,
          'country_code_2': 'SV'
        },
        {
          'name': 'Equatorial Guinea',
          'code': '+240',
          'country_code': '+240',
          'name_code': 'Equatorial Guinea: +240',
          'flag': `${flagPath}/gq.svg`,
          'country_code_2': 'GQ'
        },
        {
          'name': 'Eritrea',
          'code': '+291',
          'country_code': '+291',
          'name_code': 'Eritrea: +291',
          'flag': `${flagPath}/er.svg`,
          'country_code_2': 'ER'
        },
        {
          'name': 'Estonia',
          'code': '+372',
          'country_code': '+372',
          'name_code': 'Estonia: +372',
          'flag': `${flagPath}/ee.svg`,
          'country_code_2': 'EE'
        },
        {
          'name': 'Ethiopia',
          'code': '+251',
          'country_code': '+251',
          'name_code': 'Ethiopia: +251',
          'flag': `${flagPath}/et.svg`,
          'country_code_2': 'ET'
        },
        {
          'name': 'Falkland Islands (Malvinas)',
          'code': '+500',
          'country_code': '+500',
          'name_code': 'Falkland Islands (Malvinas): +500',
          'flag': `${flagPath}/fk.svg`,
          'country_code_2': 'FK'
        },
        {
          'name': 'Faroe Islands',
          'code': '+298',
          'country_code': '+298',
          'name_code': 'Faroe Islands: +298',
          'flag': `${flagPath}/fo.svg`,
          'country_code_2': 'FO'
        },
        {
          'name': 'Fiji',
          'code': '+679',
          'country_code': '+679',
          'name_code': 'Fiji: +679',
          'flag': `${flagPath}/fj.svg`,
          'country_code_2': 'FJ'
        },
        {
          'name': 'Finland',
          'code': '+358',
          'country_code': '+358',
          'name_code': 'Finland: +358',
          'flag': `${flagPath}/fi.svg`,
          'country_code_2': 'FI'
        },
        {
          'name': 'France',
          'code': '+33',
          'country_code': '+33',
          'name_code': 'France: +33',
          'flag': `${flagPath}/fr.svg`,
          'country_code_2': 'FR'
        },
        {
          'name': 'French Guiana',
          'code': '+594',
          'country_code': '+594',
          'name_code': 'French Guiana: +594',
          'flag': `${flagPath}/gf.svg`,
          'country_code_2': 'GF'
        },
        {
          'name': 'French Polynesia',
          'code': '+689',
          'country_code': '+689',
          'name_code': 'French Polynesia: +689',
          'flag': `${flagPath}/pf.svg`,
          'country_code_2': 'PF'
        },
        {
          'name': 'Gabon',
          'code': '+241',
          'country_code': '+241',
          'name_code': 'Gabon: +241',
          'flag': `${flagPath}/ga.svg`,
          'country_code_2': 'GA'
        },
        {
          'name': 'Gambia',
          'code': '+220',
          'country_code': '+220',
          'name_code': 'Gambia: +220',
          'flag': `${flagPath}/gm.svg`,
          'country_code_2': 'GM'
        },
        {
          'name': 'Georgia',
          'code': '+995',
          'country_code': '+995',
          'name_code': 'Georgia: +995',
          'flag': `${flagPath}/ge.svg`,
          'country_code_2': 'GE'
        },
        {
          'name': 'Germany',
          'code': '+49',
          'country_code': '+49',
          'name_code': 'Germany: +49',
          'flag': `${flagPath}/de.svg`,
          'country_code_2': 'DE'
        },
        {
          'name': 'Ghana',
          'code': '+233',
          'country_code': '+233',
          'name_code': 'Ghana: +233',
          'flag': `${flagPath}/gh.svg`,
          'country_code_2': 'GH'
        },
        {
          'name': 'Gibraltar',
          'code': '+350',
          'country_code': '+350',
          'name_code': 'Gibraltar: +350',
          'flag': `${flagPath}/gi.svg`,
          'country_code_2': 'GI'
        },
        {
          'name': 'Greece',
          'code': '+30',
          'country_code': '+30',
          'name_code': 'Greece: +30',
          'flag': `${flagPath}/gr.svg`,
          'country_code_2': 'GR'
        },
        {
          'name': 'Greenland',
          'code': '+299',
          'country_code': '+299',
          'name_code': 'Greenland: +299',
          'flag': `${flagPath}/gl.svg`,
          'country_code_2': 'GL'
        },
        {
          'name': 'Grenada',
          'code': '+1473',
          'country_code': '+1473',
          'name_code': 'Grenada: +1473',
          'flag': `${flagPath}/gd.svg`,
          'country_code_2': 'GD'
        },
        {
          'name': 'Guadeloupe',
          'code': '+590',
          'country_code': '+590',
          'name_code': 'Guadeloupe: +590',
          'flag': `${flagPath}/gp.svg`,
          'country_code_2': 'GP'
        },
        {
          'name': 'Guam',
          'code': '+1671',
          'country_code': '+1671',
          'name_code': 'Guam: +1671',
          'flag': `${flagPath}/gu.svg`,
          'country_code_2': 'GU'
        },
        {
          'name': 'Guatemala',
          'code': '+502',
          'country_code': '+502',
          'name_code': 'Guatemala: +502',
          'flag': `${flagPath}/gt.svg`,
          'country_code_2': 'GT'
        },
        {
          'name': 'Guernsey',
          'code': '+44',
          'country_code': '+44',
          'name_code': 'Guernsey: +44',
          'flag': `${flagPath}/gg.svg`,
          'country_code_2': 'GG'
        },
        {
          'name': 'Guinea',
          'code': '+224',
          'country_code': '+224',
          'name_code': 'Guinea: +224',
          'flag': `${flagPath}/gn.svg`,
          'country_code_2': 'GN'
        },
        {
          'name': 'Guinea-Bissau',
          'code': '+245',
          'country_code': '+245',
          'name_code': 'Guinea-Bissau: +245',
          'flag': `${flagPath}/gw.svg`,
          'country_code_2': 'GW'
        },
        {
          'name': 'Guyana',
          'code': '+595',
          'country_code': '+595',
          'name_code': 'Guyana: +595',
          'flag': `${flagPath}/gy.svg`,
          'country_code_2': 'GY'
        },
        {
          'name': 'Haiti',
          'code': '+509',
          'country_code': '+509',
          'name_code': 'Haiti: +509',
          'flag': `${flagPath}/ht.svg`,
          'country_code_2': 'HT'
        },
        {
          'name': 'Holy See (Vatican City State)',
          'code': '+379',
          'country_code': '+379',
          'name_code': 'Holy See (Vatican City State): +379',
          'flag': `${flagPath}/va.svg`,
          'country_code_2': 'VA'
        },
        {
          'name': 'Honduras',
          'code': '+504',
          'country_code': '+504',
          'name_code': 'Honduras: +504',
          'flag': `${flagPath}/hn.svg`,
          'country_code_2': 'HN'
        },
        {
          'name': 'Hong Kong',
          'code': '+852',
          'country_code': '+852',
          'name_code': 'Hong Kong: +852',
          'flag': `${flagPath}/hk.svg`,
          'country_code_2': 'HK'
        },
        {
          'name': 'Hungary',
          'code': '+36',
          'country_code': '+36',
          'name_code': 'Hungary: +36',
          'flag': `${flagPath}/hu.svg`,
          'country_code_2': 'HU'
        },
        {
          'name': 'Iceland',
          'code': '+354',
          'country_code': '+354',
          'name_code': 'Iceland: +354',
          'flag': `${flagPath}/is.svg`,
          'country_code_2': 'IS'
        },
        {
          'name': 'India',
          'code': '+91',
          'country_code': '+91',
          'name_code': 'India: +91',
          'flag': `${flagPath}/in.svg`,
          'country_code_2': 'IN'
        },
        {
          'name': 'Indonesia',
          'code': '+62',
          'country_code': '+62',
          'name_code': 'Indonesia: +62',
          'flag': `${flagPath}/id.svg`,
          'country_code_2': 'ID'
        },
        {
          'name': 'Iran, Islamic Republic of',
          'code': '+98',
          'country_code': '+98',
          'name_code': 'Iran, Islamic Republic of: +98',
          'flag': `${flagPath}/ir.svg`,
          'country_code_2': 'IR'
        },
        {
          'name': 'Iraq',
          'code': '+964',
          'country_code': '+964',
          'name_code': 'Iraq: +964',
          'flag': `${flagPath}/iq.svg`,
          'country_code_2': 'IQ'
        },
        {
          'name': 'Ireland',
          'code': '+353',
          'country_code': '+353',
          'name_code': 'Ireland: +353',
          'flag': `${flagPath}/ie.svg`,
          'country_code_2': 'IE'
        },
        {
          'name': 'Isle of Man',
          'code': '+44',
          'country_code': '+44',
          'name_code': 'Isle of Man: +44',
          'flag': `${flagPath}/im.svg`,
          'country_code_2': 'IM'
        },
        {
          'name': 'Israel',
          'code': '+972',
          'country_code': '+972',
          'name_code': 'Israel: +972',
          'flag': `${flagPath}/il.svg`,
          'country_code_2': 'IL'
        },
        {
          'name': 'Italy',
          'code': '+39',
          'country_code': '+39',
          'name_code': 'Italy: +39',
          'flag': `${flagPath}/it.svg`,
          'country_code_2': 'IT'
        },
        {
          'name': 'Jamaica',
          'code': '+1876',
          'country_code': '+1876',
          'name_code': 'Jamaica: +1876',
          'flag': `${flagPath}/jm.svg`,
          'country_code_2': 'JM'
        },
        {
          'name': 'Japan',
          'code': '+81',
          'country_code': '+81',
          'name_code': 'Japan: +81',
          'flag': `${flagPath}/jp.svg`,
          'country_code_2': 'JP'
        },
        {
          'name': 'Jersey',
          'code': '+44',
          'country_code': '+44',
          'name_code': 'Jersey: +44',
          'flag': `${flagPath}/je.svg`,
          'country_code_2': 'JE'
        },
        {
          'name': 'Jordan',
          'code': '+962',
          'country_code': '+962',
          'name_code': 'Jordan: +962',
          'flag': `${flagPath}/jo.svg`,
          'country_code_2': 'JO'
        },
        {
          'name': 'Kazakhstan',
          'code': '+7',
          'country_code': '+7',
          'name_code': 'Kazakhstan: +7',
          'flag': `${flagPath}/kz.svg`,
          'country_code_2': 'KZ'
        },
        {
          'name': 'Kenya',
          'code': '+254',
          'country_code': '+254',
          'name_code': 'Kenya: +254',
          'flag': `${flagPath}/ke.svg`,
          'country_code_2': 'KE'
        },
        {
          'name': 'Kiribati',
          'code': '+686',
          'country_code': '+686',
          'name_code': 'Kiribati: +686',
          'flag': `${flagPath}/ki.svg`,
          'country_code_2': 'KI'
        },
        {
          'name': "Korea, Democratic People's Republic of",
          'code': '+850',
          'country_code': '+850',
          'name_code': "Korea, Democratic People's Republic of: +850",
          'flag': `${flagPath}/kp.svg`,
          'country_code_2': 'KP'
        },
        {
          'name': 'Korea, Republic of',
          'code': '+82',
          'country_code': '+82',
          'name_code': 'Korea, Republic of: +82',
          'flag': `${flagPath}/kr.svg`,
          'country_code_2': 'KR'
        },
        {
          'name': 'Kuwait',
          'code': '+965',
          'country_code': '+965',
          'name_code': 'Kuwait: +965',
          'flag': `${flagPath}/kw.svg`,
          'country_code_2': 'KW'
        },
        {
          'name': 'Kyrgyzstan',
          'code': '+996',
          'country_code': '+996',
          'name_code': 'Kyrgyzstan: +996',
          'flag': `${flagPath}/kg.svg`,
          'country_code_2': 'KG'
        },
        {
          'name': "Lao People's Democratic Republic",
          'code': '+856',
          'country_code': '+856',
          'name_code': "Lao People's Democratic Republic: +856",
          'flag': `${flagPath}/la.svg`,
          'country_code_2': 'LA'
        },
        {
          'name': 'Latvia',
          'code': '+371',
          'country_code': '+371',
          'name_code': 'Latvia: +371',
          'flag': `${flagPath}/lv.svg`,
          'country_code_2': 'LV'
        },
        {
          'name': 'Lebanon',
          'code': '+961',
          'country_code': '+961',
          'name_code': 'Lebanon: +961',
          'flag': `${flagPath}/lb.svg`,
          'country_code_2': 'LB'
        },
        {
          'name': 'Lesotho',
          'code': '+266',
          'country_code': '+266',
          'name_code': 'Lesotho: +266',
          'flag': `${flagPath}/ls.svg`,
          'country_code_2': 'LS'
        },
        {
          'name': 'Liberia',
          'code': '+231',
          'country_code': '+231',
          'name_code': 'Liberia: +231',
          'flag': `${flagPath}/lr.svg`,
          'country_code_2': 'LR'
        },
        {
          'name': 'Libyan Arab Jamahiriya',
          'code': '+218',
          'country_code': '+218',
          'name_code': 'Libyan Arab Jamahiriya: +218',
          'flag': `${flagPath}/ly.svg`,
          'country_code_2': 'LY'
        },
        {
          'name': 'Liechtenstein',
          'code': '+423',
          'country_code': '+423',
          'name_code': 'Liechtenstein: +423',
          'flag': `${flagPath}/li.svg`,
          'country_code_2': 'LI'
        },
        {
          'name': 'Lithuania',
          'code': '+370',
          'country_code': '+370',
          'name_code': 'Lithuania: +370',
          'flag': `${flagPath}/lt.svg`,
          'country_code_2': 'LT'
        },
        {
          'name': 'Luxembourg',
          'code': '+352',
          'country_code': '+352',
          'name_code': 'Luxembourg: +352',
          'flag': `${flagPath}/lu.svg`,
          'country_code_2': 'LU'
        },
        {
          'name': 'Macao',
          'code': '+853',
          'country_code': '+853',
          'name_code': 'Macao: +853',
          'flag': `${flagPath}/mo.svg`,
          'country_code_2': 'MO'
        },
        {
          'name': 'Macedonia, The Former Yugoslav Republic of',
          'code': '+389',
          'country_code': '+389',
          'name_code': 'Macedonia, The Former Yugoslav Republic of: +389',
          'flag': `${flagPath}/mk.svg`,
          'country_code_2': 'MK'
        },
        {
          'name': 'Madagascar',
          'code': '+261',
          'country_code': '+261',
          'name_code': 'Madagascar: +261',
          'flag': `${flagPath}/mg.svg`,
          'country_code_2': 'MG'
        },
        {
          'name': 'Malawi',
          'code': '+265',
          'country_code': '+265',
          'name_code': 'Malawi: +265',
          'flag': `${flagPath}/mw.svg`,
          'country_code_2': 'MW'
        },
        {
          'name': 'Malaysia',
          'code': '+60',
          'country_code': '+60',
          'name_code': 'Malaysia: +60',
          'flag': `${flagPath}/my.svg`,
          'country_code_2': 'MY'
        },
        {
          'name': 'Maldives',
          'code': '+960',
          'country_code': '+960',
          'name_code': 'Maldives: +960',
          'flag': `${flagPath}/mv.svg`,
          'country_code_2': 'MV'
        },
        {
          'name': 'Mali',
          'code': '+223',
          'country_code': '+223',
          'name_code': 'Mali: +223',
          'flag': `${flagPath}/ml.svg`,
          'country_code_2': 'ML'
        },
        {
          'name': 'Malta',
          'code': '+356',
          'country_code': '+356',
          'name_code': 'Malta: +356',
          'flag': `${flagPath}/mt.svg`,
          'country_code_2': 'MT'
        },
        {
          'name': 'Marshall Islands',
          'code': '+692',
          'country_code': '+692',
          'name_code': 'Marshall Islands: +692',
          'flag': `${flagPath}/mh.svg`,
          'country_code_2': 'MH'
        },
        {
          'name': 'Martinique',
          'code': '+596',
          'country_code': '+596',
          'name_code': 'Martinique: +596',
          'flag': `${flagPath}/mq.svg`,
          'country_code_2': 'MQ'
        },
        {
          'name': 'Mauritania',
          'code': '+222',
          'country_code': '+222',
          'name_code': 'Mauritania: +222',
          'flag': `${flagPath}/mr.svg`,
          'country_code_2': 'MR'
        },
        {
          'name': 'Mauritius',
          'code': '+230',
          'country_code': '+230',
          'name_code': 'Mauritius: +230',
          'flag': `${flagPath}/mu.svg`,
          'country_code_2': 'MU'
        },
        {
          'name': 'Mayotte',
          'code': '+262',
          'country_code': '+262',
          'name_code': 'Mayotte: +262',
          'flag': `${flagPath}/yt.svg`,
          'country_code_2': 'YT'
        },
        {
          'name': 'Mexico',
          'code': '+52',
          'country_code': '+52',
          'name_code': 'Mexico: +52',
          'flag': `${flagPath}/mx.svg`,
          'country_code_2': 'MX'
        },
        {
          'name': 'Micronesia, Federated States of',
          'code': '+691',
          'country_code': '+691',
          'name_code': 'Micronesia, Federated States of: +691',
          'flag': `${flagPath}/fm.svg`,
          'country_code_2': 'FM'
        },
        {
          'name': 'Moldova, Republic of',
          'code': '+373',
          'country_code': '+373',
          'name_code': 'Moldova, Republic of: +373',
          'flag': `${flagPath}/md.svg`,
          'country_code_2': 'MD'
        },
        {
          'name': 'Monaco',
          'code': '+377',
          'country_code': '+377',
          'name_code': 'Monaco: +377',
          'flag': `${flagPath}/mc.svg`,
          'country_code_2': 'MC'
        },
        {
          'name': 'Mongolia',
          'code': '+976',
          'country_code': '+976',
          'name_code': 'Mongolia: +976',
          'flag': `${flagPath}/mn.svg`,
          'country_code_2': 'MN'
        },
        {
          'name': 'Montenegro',
          'code': '+382',
          'country_code': '+382',
          'name_code': 'Montenegro: +382',
          'flag': `${flagPath}/me.svg`,
          'country_code_2': 'ME'
        },
        {
          'name': 'Montserrat',
          'code': '+1664',
          'country_code': '+1664',
          'name_code': 'Montserrat: +1664',
          'flag': `${flagPath}/ms.svg`,
          'country_code_2': 'MS'
        },
        {
          'name': 'Morocco',
          'code': '+212',
          'country_code': '+212',
          'name_code': 'Morocco: +212',
          'flag': `${flagPath}/ma.svg`,
          'country_code_2': 'MA'
        },
        {
          'name': 'Mozambique',
          'code': '+258',
          'country_code': '+258',
          'name_code': 'Mozambique: +258',
          'flag': `${flagPath}/mz.svg`,
          'country_code_2': 'MZ'
        },
        {
          'name': 'Myanmar',
          'code': '+95',
          'country_code': '+95',
          'name_code': 'Myanmar: +95',
          'flag': `${flagPath}/mm.svg`,
          'country_code_2': 'MM'
        },
        {
          'name': 'Namibia',
          'code': '+264',
          'country_code': '+264',
          'name_code': 'Namibia: +264',
          'flag': `${flagPath}/na.svg`,
          'country_code_2': 'NA'
        },
        {
          'name': 'Nauru',
          'code': '+674',
          'country_code': '+674',
          'name_code': 'Nauru: +674',
          'flag': `${flagPath}/nr.svg`,
          'country_code_2': 'NR'
        },
        {
          'name': 'Nepal',
          'code': '+977',
          'country_code': '+977',
          'name_code': 'Nepal: +977',
          'flag': `${flagPath}/np.svg`,
          'country_code_2': 'NP'
        },
        {
          'name': 'Netherlands',
          'code': '+31',
          'country_code': '+31',
          'name_code': 'Netherlands: +31',
          'flag': `${flagPath}/nl.svg`,
          'country_code_2': 'NL'
        },
        {
          'name': 'Netherlands Antilles',
          'code': '+599',
          'country_code': '+599',
          'name_code': 'Netherlands Antilles: +599',
          'flag': `${flagPath}/an.svg`,
          'country_code_2': 'AN'
        },
        {
          'name': 'New Caledonia',
          'code': '+687',
          'country_code': '+687',
          'name_code': 'New Caledonia: +687',
          'flag': `${flagPath}/nc.svg`,
          'country_code_2': 'NC'
        },
        {
          'name': 'New Zealand',
          'code': '+64',
          'country_code': '+64',
          'name_code': 'New Zealand: +64',
          'flag': `${flagPath}/nz.svg`,
          'country_code_2': 'NZ'
        },
        {
          'name': 'Nicaragua',
          'code': '+505',
          'country_code': '+505',
          'name_code': 'Nicaragua: +505',
          'flag': `${flagPath}/ni.svg`,
          'country_code_2': 'NI'
        },
        {
          'name': 'Niger',
          'code': '+227',
          'country_code': '+227',
          'name_code': 'Niger: +227',
          'flag': `${flagPath}/ne.svg`,
          'country_code_2': 'NE'
        },
        {
          'name': 'Nigeria',
          'code': '+234',
          'country_code': '+234',
          'name_code': 'Nigeria: +234',
          'flag': `${flagPath}/ng.svg`,
          'country_code_2': 'NG'
        },
        {
          'name': 'Niue',
          'code': '+683',
          'country_code': '+683',
          'name_code': 'Niue: +683',
          'flag': `${flagPath}/nu.svg`,
          'country_code_2': 'NU'
        },
        {
          'name': 'Norfolk Island',
          'code': '+672',
          'country_code': '+672',
          'name_code': 'Norfolk Island: +672',
          'flag': `${flagPath}/nf.svg`,
          'country_code_2': 'NF'
        },
        {
          'name': 'Northern Mariana Islands',
          'code': '+1670',
          'country_code': '+1670',
          'name_code': 'Northern Mariana Islands: +1670',
          'flag': `${flagPath}/mp.svg`,
          'country_code_2': 'MP'
        },
        {
          'name': 'Norway',
          'code': '+47',
          'country_code': '+47',
          'name_code': 'Norway: +47',
          'flag': `${flagPath}/no.svg`,
          'country_code_2': 'NO'
        },
        {
          'name': 'Oman',
          'code': '+968',
          'country_code': '+968',
          'name_code': 'Oman: +968',
          'flag': `${flagPath}/om.svg`,
          'country_code_2': 'OM'
        },
        {
          'name': 'Pakistan',
          'code': '+92',
          'country_code': '+92',
          'name_code': 'Pakistan: +92',
          'flag': `${flagPath}/pk.svg`,
          'country_code_2': 'PK'
        },
        {
          'name': 'Palau',
          'code': '+680',
          'country_code': '+680',
          'name_code': 'Palau: +680',
          'flag': `${flagPath}/pw.svg`,
          'country_code_2': 'PW'
        },
        {
          'name': 'Palestinian Territory, Occupied',
          'code': '+970',
          'country_code': '+970',
          'name_code': 'Palestinian Territory, Occupied: +970',
          'flag': `${flagPath}/ps.svg`,
          'country_code_2': 'PS'
        },
        {
          'name': 'Panama',
          'code': '+507',
          'country_code': '+507',
          'name_code': 'Panama: +507',
          'flag': `${flagPath}/pa.svg`,
          'country_code_2': 'PA'
        },
        {
          'name': 'Papua New Guinea',
          'code': '+675',
          'country_code': '+675',
          'name_code': 'Papua New Guinea: +675',
          'flag': `${flagPath}/pg.svg`,
          'country_code_2': 'PG'
        },
        {
          'name': 'Paraguay',
          'code': '+595',
          'country_code': '+595',
          'name_code': 'Paraguay: +595',
          'flag': `${flagPath}/py.svg`,
          'country_code_2': 'PY'
        },
        {
          'name': 'Peru',
          'code': '+51',
          'country_code': '+51',
          'name_code': 'Peru: +51',
          'flag': `${flagPath}/pe.svg`,
          'country_code_2': 'PE'
        },
        {
          'name': 'Philippines',
          'code': '+63',
          'country_code': '+63',
          'name_code': 'Philippines: +63',
          'flag': `${flagPath}/ph.svg`,
          'country_code_2': 'PH'
        },
        {
          'name': 'Pitcairn',
          'code': '+872',
          'country_code': '+872',
          'name_code': 'Pitcairn: +872',
          'flag': `${flagPath}/pn.svg`,
          'country_code_2': 'PN'
        },
        {
          'name': 'Poland',
          'code': '+48',
          'country_code': '+48',
          'name_code': 'Poland: +48',
          'flag': `${flagPath}/pl.svg`,
          'country_code_2': 'PL'
        },
        {
          'name': 'Portugal',
          'code': '+351',
          'country_code': '+351',
          'name_code': 'Portugal: +351',
          'flag': `${flagPath}/pt.svg`,
          'country_code_2': 'PT'
        },
        {
          'name': 'Puerto Rico',
          'code': '+1939',
          'country_code': '+1939',
          'name_code': 'Puerto Rico: +1939',
          'flag': `${flagPath}/pr.svg`,
          'country_code_2': 'PR'
        },
        {
          'name': 'Qatar',
          'code': '+974',
          'country_code': '+974',
          'name_code': 'Qatar: +974',
          'flag': `${flagPath}/qa.svg`,
          'country_code_2': 'QA'
        },
        {
          'name': 'Romania',
          'code': '+40',
          'country_code': '+40',
          'name_code': 'Romania: +40',
          'flag': `${flagPath}/ro.svg`,
          'country_code_2': 'RO'
        },
        {
          'name': 'Russia',
          'code': '+7',
          'country_code': '+7',
          'name_code': 'Russia: +7',
          'flag': `${flagPath}/ru.svg`,
          'country_code_2': 'RU'
        },
        {
          'name': 'Rwanda',
          'code': '+250',
          'country_code': '+250',
          'name_code': 'Rwanda: +250',
          'flag': `${flagPath}/rw.svg`,
          'country_code_2': 'RW'
        },
        {
          'name': 'Réunion',
          'code': '+262',
          'country_code': '+262',
          'name_code': 'Réunion: +262',
          'flag': `${flagPath}/re.svg`,
          'country_code_2': 'RE'
        },
        {
          'name': 'Saint Barthélemy',
          'code': '+590',
          'country_code': '+590',
          'name_code': 'Saint Barthélemy: +590',
          'flag': `${flagPath}/bl.svg`,
          'country_code_2': 'BL'
        },
        {
          'name': 'Saint Helena, Ascension and Tristan Da Cunha',
          'code': '+290',
          'country_code': '+290',
          'name_code': 'Saint Helena, Ascension and Tristan Da Cunha: +290',
          'flag': `${flagPath}/sh.svg`,
          'country_code_2': 'SH'
        },
        {
          'name': 'Saint Kitts and Nevis',
          'code': '+1869',
          'country_code': '+1869',
          'name_code': 'Saint Kitts and Nevis: +1869',
          'flag': `${flagPath}/kn.svg`,
          'country_code_2': 'KN'
        },
        {
          'name': 'Saint Lucia',
          'code': '+1758',
          'country_code': '+1758',
          'name_code': 'Saint Lucia: +1758',
          'flag': `${flagPath}/lc.svg`,
          'country_code_2': 'LC'
        },
        {
          'name': 'Saint Martin',
          'code': '+590',
          'country_code': '+590',
          'name_code': 'Saint Martin: +590',
          'flag': `${flagPath}/mf.svg`,
          'country_code_2': 'MF'
        },
        {
          'name': 'Saint Pierre and Miquelon',
          'code': '+508',
          'country_code': '+508',
          'name_code': 'Saint Pierre and Miquelon: +508',
          'flag': `${flagPath}/pm.svg`,
          'country_code_2': 'PM'
        },
        {
          'name': 'Saint Vincent and the Grenadines',
          'code': '+1784',
          'country_code': '+1784',
          'name_code': 'Saint Vincent and the Grenadines: +1784',
          'flag': `${flagPath}/vc.svg`,
          'country_code_2': 'VC'
        },
        {
          'name': 'Samoa',
          'code': '+685',
          'country_code': '+685',
          'name_code': 'Samoa: +685',
          'flag': `${flagPath}/ws.svg`,
          'country_code_2': 'WS'
        },
        {
          'name': 'San Marino',
          'code': '+378',
          'country_code': '+378',
          'name_code': 'San Marino: +378',
          'flag': `${flagPath}/sm.svg`,
          'country_code_2': 'SM'
        },
        {
          'name': 'Sao Tome and Principe',
          'code': '+239',
          'country_code': '+239',
          'name_code': 'Sao Tome and Principe: +239',
          'flag': `${flagPath}/st.svg`,
          'country_code_2': 'ST'
        },
        {
          'name': 'Saudi Arabia',
          'code': '+966',
          'country_code': '+966',
          'name_code': 'Saudi Arabia: +966',
          'flag': `${flagPath}/sa.svg`,
          'country_code_2': 'SA'
        },
        {
          'name': 'Senegal',
          'code': '+221',
          'country_code': '+221',
          'name_code': 'Senegal: +221',
          'flag': `${flagPath}/sn.svg`,
          'country_code_2': 'SN'
        },
        {
          'name': 'Serbia',
          'code': '+381',
          'country_code': '+381',
          'name_code': 'Serbia: +381',
          'flag': `${flagPath}/rs.svg`,
          'country_code_2': 'RS'
        },
        {
          'name': 'Seychelles',
          'code': '+248',
          'country_code': '+248',
          'name_code': 'Seychelles: +248',
          'flag': `${flagPath}/sc.svg`,
          'country_code_2': 'SC'
        },
        {
          'name': 'Sierra Leone',
          'code': '+232',
          'country_code': '+232',
          'name_code': 'Sierra Leone: +232',
          'flag': `${flagPath}/sl.svg`,
          'country_code_2': 'SL'
        },
        {
          'name': 'Singapore',
          'code': '+65',
          'country_code': '+65',
          'name_code': 'Singapore: +65',
          'flag': `${flagPath}/sg.svg`,
          'country_code_2': 'SG'
        },
        {
          'name': 'Slovakia',
          'code': '+421',
          'country_code': '+421',
          'name_code': 'Slovakia: +421',
          'flag': `${flagPath}/sk.svg`,
          'country_code_2': 'SK'
        },
        {
          'name': 'Slovenia',
          'code': '+386',
          'country_code': '+386',
          'name_code': 'Slovenia: +386',
          'flag': `${flagPath}/si.svg`,
          'country_code_2': 'SI'
        },
        {
          'name': 'Solomon Islands',
          'code': '+677',
          'country_code': '+677',
          'name_code': 'Solomon Islands: +677',
          'flag': `${flagPath}/sb.svg`,
          'country_code_2': 'SB'
        },
        {
          'name': 'Somalia',
          'code': '+252',
          'country_code': '+252',
          'name_code': 'Somalia: +252',
          'flag': `${flagPath}/so.svg`,
          'country_code_2': 'SO'
        },
        {
          'name': 'South Africa',
          'code': '+27',
          'country_code': '+27',
          'name_code': 'South Africa: +27',
          'flag': `${flagPath}/za.svg`,
          'country_code_2': 'ZA'
        },
        {
          'name': 'South Georgia and the South Sandwich Islands',
          'code': '+500',
          'country_code': '+500',
          'name_code': 'South Georgia and the South Sandwich Islands: +500',
          'flag': `${flagPath}/gs.svg`,
          'country_code_2': 'GS'
        },
        {
          'name': 'Spain',
          'code': '+34',
          'country_code': '+34',
          'name_code': 'Spain: +34',
          'flag': `${flagPath}/es.svg`,
          'country_code_2': 'ES'
        },
        {
          'name': 'Sri Lanka',
          'code': '+94',
          'country_code': '+94',
          'name_code': 'Sri Lanka: +94',
          'flag': `${flagPath}/lk.svg`,
          'country_code_2': 'LK'
        },
        {
          'name': 'Sudan',
          'code': '+249',
          'country_code': '+249',
          'name_code': 'Sudan: +249',
          'flag': `${flagPath}/sd.svg`,
          'country_code_2': 'SD'
        },
        {
          'name': 'Suriname',
          'code': '+597',
          'country_code': '+597',
          'name_code': 'Suriname: +597',
          'flag': `${flagPath}/sr.svg`,
          'country_code_2': 'SR'
        },
        {
          'name': 'Svalbard and Jan Mayen',
          'code': '+47',
          'country_code': '+47',
          'name_code': 'Svalbard and Jan Mayen: +47',
          'flag': `${flagPath}/sj.svg`,
          'country_code_2': 'SJ'
        },
        {
          'name': 'Swaziland',
          'code': '+268',
          'country_code': '+268',
          'name_code': 'Swaziland: +268',
          'flag': `${flagPath}/sz.svg`,
          'country_code_2': 'SZ'
        },
        {
          'name': 'Sweden',
          'code': '+46',
          'country_code': '+46',
          'name_code': 'Sweden: +46',
          'flag': `${flagPath}/se.svg`,
          'country_code_2': 'SE'
        },
        {
          'name': 'Switzerland',
          'code': '+41',
          'country_code': '+41',
          'name_code': 'Switzerland: +41',
          'flag': `${flagPath}/ch.svg`,
          'country_code_2': 'CH'
        },
        {
          'name': 'Syrian Arab Republic',
          'code': '+963',
          'country_code': '+963',
          'name_code': 'Syrian Arab Republic: +963',
          'flag': `${flagPath}/sy.svg`,
          'country_code_2': 'SY'
        },
        {
          'name': 'Taiwan, Province of China',
          'code': '+886',
          'country_code': '+886',
          'name_code': 'Taiwan, Province of China: +886',
          'flag': `${flagPath}/tw.svg`,
          'country_code_2': 'TW'
        },
        {
          'name': 'Tajikistan',
          'code': '+992',
          'country_code': '+992',
          'name_code': 'Tajikistan: +992',
          'flag': `${flagPath}/tj.svg`,
          'country_code_2': 'TJ'
        },
        {
          'name': 'Tanzania, United Republic of',
          'code': '+255',
          'country_code': '+255',
          'name_code': 'Tanzania, United Republic of: +255',
          'flag': `${flagPath}/tz.svg`,
          'country_code_2': 'TZ'
        },
        {
          'name': 'Thailand',
          'code': '+66',
          'country_code': '+66',
          'name_code': 'Thailand: +66',
          'flag': `${flagPath}/th.svg`,
          'country_code_2': 'TH'
        },
        {
          'name': 'Timor-Leste',
          'code': '+670',
          'country_code': '+670',
          'name_code': 'Timor-Leste: +670',
          'flag': `${flagPath}/tl.svg`,
          'country_code_2': 'TL'
        },
        {
          'name': 'Togo',
          'code': '+228',
          'country_code': '+228',
          'name_code': 'Togo: +228',
          'flag': `${flagPath}/tg.svg`,
          'country_code_2': 'TG'
        },
        {
          'name': 'Tokelau',
          'code': '+690',
          'country_code': '+690',
          'name_code': 'Tokelau: +690',
          'flag': `${flagPath}/tk.svg`,
          'country_code_2': 'TK'
        },
        {
          'name': 'Tonga',
          'code': '+676',
          'country_code': '+676',
          'name_code': 'Tonga: +676',
          'flag': `${flagPath}/to.svg`,
          'country_code_2': 'TO'
        },
        {
          'name': 'Trinidad and Tobago',
          'code': '+1868',
          'country_code': '+1868',
          'name_code': 'Trinidad and Tobago: +1868',
          'flag': `${flagPath}/tt.svg`,
          'country_code_2': 'TT'
        },
        {
          'name': 'Tunisia',
          'code': '+216',
          'country_code': '+216',
          'name_code': 'Tunisia: +216',
          'flag': `${flagPath}/tn.svg`,
          'country_code_2': 'TN'
        },
        {
          'name': 'Turkey',
          'code': '+90',
          'country_code': '+90',
          'name_code': 'Turkey: +90',
          'flag': `${flagPath}/tr.svg`,
          'country_code_2': 'TR'
        },
        {
          'name': 'Turkmenistan',
          'code': '+993',
          'country_code': '+993',
          'name_code': 'Turkmenistan: +993',
          'flag': `${flagPath}/tm.svg`,
          'country_code_2': 'TM'
        },
        {
          'name': 'Turks and Caicos Islands',
          'code': '+1649',
          'country_code': '+1649',
          'name_code': 'Turks and Caicos Islands: +1649',
          'flag': `${flagPath}/tc.svg`,
          'country_code_2': 'TC'
        },
        {
          'name': 'Tuvalu',
          'code': '+688',
          'country_code': '+688',
          'name_code': 'Tuvalu: +688',
          'flag': `${flagPath}/tv.svg`,
          'country_code_2': 'TV'
        },
        {
          'name': 'Uganda',
          'code': '+256',
          'country_code': '+256',
          'name_code': 'Uganda: +256',
          'flag': `${flagPath}/ug.svg`,
          'country_code_2': 'UG'
        },
        {
          'name': 'Ukraine',
          'code': '+380',
          'country_code': '+380',
          'name_code': 'Ukraine: +380',
          'flag': `${flagPath}/ua.svg`,
          'country_code_2': 'UA'
        },
        {
          'name': 'United Arab Emirates',
          'code': '+971',
          'country_code': '+971',
          'name_code': 'United Arab Emirates: +971',
          'flag': `${flagPath}/ae.svg`,
          'country_code_2': 'AE'
        },
        {
          'name': 'United Kingdom',
          'code': '+44',
          'country_code': '+44',
          'name_code': 'United Kingdom: +44',
          'flag': `${flagPath}/gb.svg`,
          'country_code_2': 'GB'
        },
        {
          'name': 'United States',
          'code': '+1',
          'country_code': '+1',
          'name_code': 'United States: +1',
          'flag': `${flagPath}/us.svg`,
          'country_code_2': 'US'
        },
        {
          'name': 'Uruguay',
          'code': '+598',
          'country_code': '+598',
          'name_code': 'Uruguay: +598',
          'flag': `${flagPath}/uy.svg`,
          'country_code_2': 'UY'
        },
        {
          'name': 'Uzbekistan',
          'code': '+998',
          'country_code': '+998',
          'name_code': 'Uzbekistan: +998',
          'flag': `${flagPath}/uz.svg`,
          'country_code_2': 'UZ'
        },
        {
          'name': 'Vanuatu',
          'code': '+678',
          'country_code': '+678',
          'name_code': 'Vanuatu: +678',
          'flag': `${flagPath}/vu.svg`,
          'country_code_2': 'VU'
        },
        {
          'name': 'Venezuela, Bolivarian Republic of',
          'code': '+58',
          'country_code': '+58',
          'name_code': 'Venezuela, Bolivarian Republic of: +58',
          'flag': `${flagPath}/ve.svg`,
          'country_code_2': 'VE'
        },
        {
          'name': 'Viet Nam',
          'code': '+84',
          'country_code': '+84',
          'name_code': 'Viet Nam: +84',
          'flag': `${flagPath}/vn.svg`,
          'country_code_2': 'VN'
        },
        {
          'name': 'Virgin Islands, British',
          'code': '+1284',
          'country_code': '+1284',
          'name_code': 'Virgin Islands, British: +1284',
          'flag': `${flagPath}/vg.svg`,
          'country_code_2': 'VG'
        },
        {
          'name': 'Virgin Islands, U.S.',
          'code': '+1340',
          'country_code': '+1340',
          'name_code': 'Virgin Islands, U.S.: +1340',
          'flag': `${flagPath}/vi.svg`,
          'country_code_2': 'VI'
        },
        {
          'name': 'Wallis and Futuna',
          'code': '+681',
          'country_code': '+681',
          'name_code': 'Wallis and Futuna: +681',
          'flag': `${flagPath}/wf.svg`,
          'country_code_2': 'WF'
        },
        {
          'name': 'Yemen',
          'code': '+967',
          'country_code': '+967',
          'name_code': 'Yemen: +967',
          'flag': `${flagPath}/ye.svg`,
          'country_code_2': 'YE'
        },
        {
          'name': 'Zambia',
          'code': '+260',
          'country_code': '+260',
          'name_code': 'Zambia: +260',
          'flag': `${flagPath}/zm.svg`,
          'country_code_2': 'ZM'
        },
        {
          'name': 'Zimbabwe',
          'code': '+263',
          'country_code': '+263',
          'name_code': 'Zimbabwe: +263',
          'flag': `${flagPath}/zw.svg`,
          'country_code_2': 'ZW'
        },
        {
          'name': 'land Islands',
          'code': '+298',
          'country_code': '+298',
          'name_code': 'land Islands: +298',
          'flag': `${flagPath}/ax.svg`,
          'country_code_2': 'AX'
        }
      ]
      return countryCodes
    }
  }
}
