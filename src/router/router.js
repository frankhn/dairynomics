import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import {
  authenticateUser,
  openModal,
  decodeToken
} from '@/utils/helperFunctions'
import routes from './index'

Vue.use(Router)
const router = new Router({
  routes,
  mode: 'history',
  scrollBehavior: (to, from, savedPosition) => {
    if (savedPosition) {
      return savedPosition
    } else if (to.hash) {
      return {
        selector: to.hash
      }
    } else {
      return { x: 0, y: 0 }
    }
  }
})
router.beforeEach((to, from, next) => {
  const matchedLength = from.matched.length
  const { isValid } = authenticateUser()
  if (isValid) {
    store.commit('toggleIsLoggedInState', true)
  } else {
    store.commit('toggleIsLoggedInState', false)
  }
  if (to.meta.isAuth && !isValid) {
    if (matchedLength) {
      store.commit('setAuthError', 'Please Log in to continue!')
      openModal('login')
      return next(false)
    }
    router.push('/', () => {
      Vue.nextTick(() => {
        openModal('login')
        store.commit('setAuthError', 'Please Log in to continue!')
      })
    })
  }
  if (to.meta.isAuth === false && isValid) {
    return next('/user-profile')
  }
  if (to.meta.isServiceProvider) {
    const { account_type: accountType } = decodeToken(
      localStorage.getItem('token')
    )
    if (accountType !== 'Service Provider') {
      return router.push('/user-profile')
    }
  }
  next()
})
export default router
