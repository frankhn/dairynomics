// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuelidate from 'vuelidate'
import App from './App.vue'
import router from './router/router'
import store from './store'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'vue-multiselect/dist/vue-multiselect.min.css'
import 'vue-select/dist/vue-select.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faUserSecret,
  faSpinner,
  faAlignLeft,
  faCheck
} from '@fortawesome/free-solid-svg-icons'
import Cloudinary from 'cloudinary-vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import Notifications from 'vue-notification'
import Meta from 'vue-meta'
import Multiselect from 'vue-multiselect'
import TextareaAutosize from 'vue-textarea-autosize'

Vue.use(TextareaAutosize)

Vue.component('multiselect', Multiselect)

Vue.use(Cloudinary, {
  configuration: {
    cloudName: 'dcsrepenv'
  }
})
var SocialSharing = require('vue-social-sharing')

Vue.use(SocialSharing)

Vue.use(require('vue-moment'))

library.add(faSpinner, faCheck)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(Vuelidate)
Vue.use(Notifications)
Vue.use(Meta)

library.add(faSpinner, faAlignLeft, faUserSecret)
Vue.config.productionTip = false

Vue.component('font-awesome-icon', FontAwesomeIcon)

/* eslint-disable no-new */
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
