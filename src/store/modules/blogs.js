import AxiosCalls from '@/utils/api/AxiosCalls'
import getRandomItem from '@/utils/performance/getRandomItem'
import router from '@/router/router'

export default {
  state: {
    blogs: [],
    randomBlog: {}
  },
  getters: {
    BLOGS (state) {
      return state.blogs
    },
    RANDOMBLOG (state) {
      if (state.randomBlog && state.randomBlog.number_of_comments) {
        return state.randomBlog
      }
    }
  },
  mutations: {
    SET_BLOGS (state, payload) {
      state.blogs = payload
    },
    SET_RANDOM_BLOG (state, payload) {
      state.randomBlog = payload
    }
  },
  actions: {
    GET_BLOGS: ({commit}) => {
      return AxiosCalls.get('api/v1/user_blogs')
        .then((response) => {
          const randomBlog = getRandomItem(response.data.data.blogs)
          commit('SET_BLOGS', response.data.data.blogs)
          commit('SET_RANDOM_BLOG', randomBlog)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    }
  }
}
