import Vuex from 'vuex'
import {createLocalVue} from '@vue/test-utils'
import individualFarm from '@/store/modules/individualFarm.js'

export const actions = {
  getFarmDetails: jest.fn()
}

const localVue = createLocalVue()
localVue.use(Vuex)

export const mockState = {
  owner: 'owner',
  location: 12,
  num_cows: 21,
  land_size: 21,
  cow_breeds: 23,
  milk_production: 12,
  photo: ['https://beta.cowsoko.com/storage/img/farm_farmplatea_1529567564_6df6f.jpg']

}

export default new Vuex.Store({
  modules: {
    individualFarm: {
      state: { farmDetails: mockState },
      getters: individualFarm.getters,
      mutations: individualFarm.mutations,
      actions: { getFarmDetails: jest.fn() }
    }
  }
})
