import AxiosCalls from '@/utils/api/AxiosCalls'

let userId = localStorage.getItem('userId')
let token = localStorage.getItem('token')

export default {
  state: {
    notifications: [],
    errorMessage: null
  },
  getters: {
    getNotifications (state) {
      return {...state}
    }
  },
  mutations: {
    updateNotifications (state, payload) {
      state.notifications = payload
    },
    updateErrorMessage (state, payload) {
      state.errorMessage = payload
    }
  },
  actions: {
    async fetchNotifications ({commit}) {
      try {
        let response = await AxiosCalls.get(`api/v1/user/${userId}/notifications`, {
          headers: {Authorization: `Bearer ${token}`}
        })
        let responseData = response.data
        if (responseData.success) {
          commit('updateNotifications', responseData.data)
        }
        if (!responseData.success) {
          commit('updateErrorMessage', response.data.message)
          if (responseData.error) commit('updateErrorMessage', responseData.error)
        }
      } catch (error) {
        commit('updateErrorMessage', error)
      }
    },
    async markAsRead ({ commit, dispatch }) {
      try {
        let response = await AxiosCalls.get(`api/v1/user/${userId}/notifications/unread`, {
          headers: {Authorization: `Bearer ${token}`}
        })
        if (response.data.success) {
          dispatch('fetchNotifications')
        }
      } catch (error) {
        commit('updateErrorMessage', error)
      }
    }
  }
}
