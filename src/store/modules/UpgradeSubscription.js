import AxiosCalls from '@/utils/api/AxiosCalls'
export default {
  state: {
    isLoading: false,
    errorMessage: '',
    paymentStatus: ''
  },
  mutations: {
    SET_LOADER (state, payload) {
      state.isLoading = payload
    },
    SET_MESSAGE (state, payload) {
      state.errorMessage = payload
    },
    SET_PAYEMENT_STATUS (state, payload) {
      state.paymentStatus = payload
    }
  },
  getters: {
    getSubscriptionInfo (state) {
      return { ...state }
    },
    getErrorMessage (state) {
      return state.errorMessage
    },
    getPaymentStatus (state) {
      return state.paymentStatus
    }
  },
  actions: {
    async makeSubscription ({ commit }, data) {
      commit('SET_LOADER', true)
      try {
        const response = await AxiosCalls.postEcommerce(
          'payments/mpesa/initiate',
          data
        )
        if (response.data.status === 200) {
          commit('SET_MESSAGE', 'upgraded your subscription')
          commit('SET_PAYEMENT_STATUS', 'success')
          commit('SET_LOADER', false)
        }
      } catch (err) {
        let message
        if (err.response.data.message === '[CBS - ] No ICCID found') {
          message =
            'This number is not registered on safaricom, or its blocked/inactive/dead.'
        } else {
          message = err.response.data.message
        }
        commit('SET_MESSAGE', message)
        commit('SET_PAYEMENT_STATUS', 'failure')
        commit('SET_LOADER', false)
      }
    },
    async resetMessage ({ commit }) {
      commit('SET_PAYEMENT_STATUS', '')
    }
  }
}
