import AxiosCalls from '@/utils/api/AxiosCalls'
import uniqueBy from 'unique-by'
import router from 'vue-router'

export default {
  state: {
    allPublications: {
      data: [],
      pagination: {},
      paginatedData: {}
    },
    featuredPublications: {
      data: [],
      pagination: {},
      paginatedData: {}
    },
    requestStatuses: {
      isDownloading: false,
      isFetching: false
    }
  },
  mutations: {
    STORE_FETCHED_PUBLICATIONS (state, { data, dataScope = 'allPublications' }) {
      const allData = uniqueBy([...state[dataScope].data, ...data], 'id')
      const allPublicationsData = uniqueBy([...state.allPublications.data, ...data], 'id')
      state[dataScope].data = allData
      state.allPublications.data = allPublicationsData
    },
    STORE_PAGINATED_PUBLICATIONS (state, { data, currentPage, dataScope = 'allPublications' }) {
      const { paginatedData } = state[dataScope]
      state[dataScope].paginatedData = {
        ...paginatedData,
        [`page-${currentPage}`]: data
      }
    },
    STORE_PAGINATION_DATA (state, { paginationData, dataScope = 'allPublications' }) {
      state[dataScope].pagination = {
        ...state[dataScope].pagination, ...paginationData
      }
    },
    CHANGE_REQUEST_STATUS (state, payload) {
      state.requestStatuses = {
        ...state.requestStatuses,
        ...payload
      }
    }
  },
  actions: {
    async fetchPublications ({ commit, state }, { pageNumber, dataScope = 'allPublications' }) {
      pageNumber = pageNumber === undefined ? 1 : pageNumber
      commit('CHANGE_REQUEST_STATUS', { isFetching: true })

      let requestsMap = {
        allPublications: 'api/v1/publications',
        featuredPublications: 'api/v1/publications/featured'
      }

      try {
        const endpoint = requestsMap[dataScope]
        const url = `${endpoint}?page=${pageNumber}`
        let { data: responseData } = await AxiosCalls.get(url)

        const {
          meta: { current_page: currentPage, last_page: lastPage, total }
        } = responseData

        commit('CHANGE_REQUEST_STATUS', { isFetching: false })
        commit('STORE_FETCHED_PUBLICATIONS', { data: responseData.data, dataScope })
        commit('STORE_PAGINATED_PUBLICATIONS', { data: responseData.data, currentPage, dataScope })

        const paginationData = {
          lastPage,
          total
        }
        /**
         * We set the currentPage to the current page returned from the backend
         * ONLY on first request.
         * subsequent changes to the currentPage property will be triggered
         * on click of the "previous" and "next" buttons on the UI
         */

        if (state[dataScope].pagination.currentPage === undefined) {
          paginationData.currentPage = currentPage
        }

        commit('STORE_PAGINATION_DATA', { paginationData, dataScope })
      } catch (error) {
        router.push('/')
      }
    },
    // async downloadPublication ({ commit }, publication) {
    //   commit('CHANGE_REQUEST_STATUS', { isDownloading: true })

    //   const token = localStorage.getItem('token')
    //   let userId = localStorage.getItem('userId')

    //   const config = {
    //     method: 'GET',
    //     headers: {
    //       Authorization: `Bearer ${token}`
    //     },
    //     data: {user_id: userId},
    //     responseType: 'blob'
    //   }

    //   try {
    //     let { data } = await AxiosCalls.get(`api/v1/publications/${publication.id}/download`, config)
    //     commit('CHANGE_REQUEST_STATUS', { isDownloading: false })

    //     const url = window.URL.createObjectURL(new Blob([data]))
    //     const link = document.createElement('a')
    //     link.href = url
    //     link.setAttribute('download', `${publication.title}.pdf`)
    //     document.body.appendChild(link)
    //     link.click()
    //   } catch (error) {
    //     commit('CHANGE_REQUEST_STATUS', { isDownloading: false })
    //   }
    // },
    incrementPage ({ commit, state }, { dataScope }) {
      let pagination = { ...state[dataScope].pagination }
      let { currentPage } = pagination
      currentPage++
      let paginationData = {
        currentPage
      }
      commit('STORE_PAGINATION_DATA', { paginationData, dataScope: 'allPublications' })
    },
    decrementPage ({ commit, state }, { dataScope }) {
      let pagination = { ...state[dataScope].pagination }
      let { currentPage } = pagination
      currentPage--
      let paginationData = {
        currentPage
      }
      commit('STORE_PAGINATION_DATA', { paginationData, dataScope: 'allPublications' })
    }
  },
  getters: {
    getPaginatedAllPublications: state => state.allPublications.paginatedData,
    getAllPublications: state => state.allPublications.data,
    getAllPublicationsPagination: state => state.allPublications.pagination,
    getPaginatedFeaturedPublications: state => state.featuredPublications.paginatedData,
    getFeaturedPublicationsPagination: state => state.featuredPublications.pagination,
    getRequestStatuses: state => state.requestStatuses
  }
}
