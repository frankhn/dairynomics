import AxiosCalls from '@/utils/api/AxiosCalls'
import getRandomItem from '@/utils/performance/getRandomItem'
import router from '@/router/router'

export default {
  state: {
    tests: [],
    randomTest: {}
  },
  getters: {
    USER_TESTS (state) {
      return state.tests
    },
    RANDOM_USER_TEST (state) {
      return state.randomTest
    }
  },
  mutations: {
    SET_USER_TESTS (state, payload) {
      state.tests = payload
    },
    SET_RANDOM_USER_TEST (state, payload) {
      state.randomTest = payload
    }
  },
  actions: {
    GET_USER_TESTS: ({commit}) => {
      return AxiosCalls.get('api/v1/onboarding_test')
        .then((response) => {
          const randomTest = getRandomItem(response.data)
          commit('SET_USER_TESTS', response.data.data)
          commit('SET_RANDOM_USER_TEST', randomTest)
        })
        .catch((error) => {
          switch (error.response.status) {
            case 404:
              return router.push('/404')
            default:
              return router.push('/')
          }
        })
    }
  }
}
