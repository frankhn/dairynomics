export default {
  state: {
    userType: null
  },

  mutations: {
    SET_USER_TYPE (state, payload) {
      state.userType = payload.userType
    }
  }
}
