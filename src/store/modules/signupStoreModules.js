/* eslint-disable handle-callback-err */
import AxiosCalls from '@/utils/api/AxiosCalls'
import { signupHelper } from '@/utils/signupHelpers.js'

export default {
  state: {
    userAuthSignUp: {
      userId: null,
      loading: false,
      success: null,
      status: null,
      errorMessage: null
    }
  },

  getters: {
    signedUpUser (state) {
      return { ...state.userAuthSignUp }
    }
  },

  actions: {
    signUpUser: async ({ commit, dispatch }, userDetails) => {
      try {
        commit('LOADER', true)
        commit('TOGGLE_SUCCESS_PROPERTY', false)

        /** [accountData]: represents data from services provider and Input sellers.
         * [path]: is the route that is being select by the user, path varies
         * according to the account selected bythe user during signup
         */
        const { path, accountData } = signupHelper(userDetails.account_type)

        const user = {
          first_name: userDetails.first_name,
          last_name: userDetails.last_name,
          phone: userDetails.phone,
          county: userDetails.county,
          password: userDetails.password,
          account_type: userDetails.account_type,
          country_code: userDetails.country_code,
          ...accountData
        }

        let { data } = await AxiosCalls.post(`/api/v1/${path}`, user)
        commit('LOADER', false)
        commit('SIGNED_UP_USER', data)
        localStorage.setItem('token', data.token)
        localStorage.setItem('userId', data.user.id)
        if (localStorage.test) {
          const test = JSON.parse(localStorage.getItem('test'))
          test.user_id = data.user.id
          dispatch('POST_TEST_RESULTS', test)
        }

        if (user.account_type.split(' ').includes('provider')) {
          dispatch('subscribeServiceProvider')
        }
      } catch (data) {
        data.success = false
        if (data.error === 422) {
          data.message = 'The Phone Number has already been taken.';
        }
        commit('SIGNED_UP_USER', data)
        commit('LOADER', false)
      }
    },

    openValidationPage ({ commit }) {
      commit('TOGGLE_SUCCESS_PROPERTY', true)
      commit('REFRESH_ERROR')
    }
  },

  mutations: {
    LOADER (state, payload) {
      state.userAuthSignUp.loading = payload
    },
    SIGNED_UP_USER (state, data) {
      if (!data.error) {
        let userId = ''
        /** The data that is returned form the api is structured differently based on the user signing up
         * this line of code accesses userId in for to differently according to its structure
         */
        data.data && data.data.id
          ? (userId = data.data.id)
          : (userId = data.user.id)
        state.userAuthSignUp.userId = userId
        state.userAuthSignUp.success = true
      } else {
        state.userAuthSignUp.errorMessage = data.message
        state.userAuthSignUp.success = false
      }
    },
    TOGGLE_SUCCESS_PROPERTY (state, payload) {
      if (state.userAuthSignUp.success) {
        state.userAuthSignUp.success = false
      }
      state.userAuthSignUp.errorMessage = null
      state.userAuthSignUp.success = payload
    }
  }
}
